from library.common.helpers import *
from library.common.LKIMHelpers import *
from library.api.auth import *
from library.api.catalogs import *
from library.api.partner import *
from library.api.aggregator import *
from library.api.adapter import *
from library.common.db import *
from slugify import slugify
from pathlib import Path
from allure_commons.types import AttachmentType
#import os


# test passing ENV
#def pytest_generate_tests(metafunc):
#    os.environ['IMAGE_TAG'] = 'ml-818'


def pytest_runtest_makereport(item, call) -> None:
    if call.when == "call":
        if call.excinfo is not None and "page" in item.funcargs:
            page = item.funcargs["page"]
            screenshot_dir = Path(".playwright-screenshots")
            screenshot_dir.mkdir(exist_ok=True)
            path = str(screenshot_dir / f"{slugify(item.nodeid)}.png")
            img = page.screenshot(path=path)
            allure.attach(img, name=path, attachment_type=AttachmentType.PNG)