from library.ui.lkim.HomePage import *
from library.ui.lkim.CabinetPage import *
from library.ui.lkim.ResetPasswordPage import *
import pytest


@allure.feature('ЛК ИМ')
@allure.story('Восстановление пароля')
@allure.title('Восстановление пароля')
def test_login_wrong_pass(page, lkim_helper, string_helper):
    email = 'passwordreset@boxberry.com'
    newPass = string_helper.get_random_string()

    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement()
    homePage.openPasswordReset()

    resetPassPage = ResetPasswordPage(page)
    resetPassPage.sendResetLink(email)
    resetLink = lkim_helper.findResetPasswordLink(email, 12)
    resetPassPage.openResetPassLink(resetLink).resetPass(newPass)

    assert 'Пароль успешно обновлен.' in resetPassPage.innerText('.password-recovery__label')

    resetPassPage.clickToMainPage()
    homePage.login(email, newPass)