import allure
import pytest
from library.common.db import *
from inspect import getmembers
from pprint import pprint


@allure.feature('Auth API')
@allure.story('Проверка получения прав к ресурсу')
@allure.title('Проверка получения прав к ресурсу')
def test_get_access_token_to_resource(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()
    resource = '/api.aggregator.Partner/AddToken'

    with allure.step('Создать токен и проверить, что доступа к ресурсу нет'):
        AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Дать доступ к ресурсу и проверить, что доступ появился'):
        AuthAPIHelper.AddTokenToRole("ta-token-" + random_str, "master")
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Отключить токен, доступ к ресурсу пропал'):
        AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Включить токен, доступ к ресурсу вернулся'):
        AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, True)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Заблокировать клиента, доступ к ресурсу пропал'):
        AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Разблокировать клиента, доступ к ресурсу вернулся'):
        AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Отобрать доступ к ресурсу, проверить что он недоступен'):
        AuthAPIHelper.RemoveTokenToRole("ta-token-" + random_str, "master")
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)