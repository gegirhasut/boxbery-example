import pika
import json
import random

rmq_url_connection_str = 'amqp://staging-ml-user:HkFJ34nhCwvt@general-mb.boxberry.ru:5672/staging-catalogs'

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

#отправка сообщения
#установка соединения

rmq_parameters = pika.URLParameters(rmq_url_connection_str)
rmq_connection = pika.BlockingConnection(rmq_parameters)
rmq_channel = rmq_connection.channel()
j = json.dumps({
                "messageID": str(random.random()),
                "enID":"5f6e0076-af90-11ea-a2ae-005056012f5e",
                "payload":{
                    "pointCode":"1003",
                    "pointsProperties":{

                    }
                }
            })
rmq_channel.basic_publish(exchange='exchange.parcel.process', routing_key='queue.parcel.point.update',body=j)


# #получение сообщений
def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)

rmq_channel.basic_consume(queue='queue.parcel.point.update.response',
                      auto_ack=True,
                      on_message_callback=callback)

print(' [*] Waiting for messages. To exit press CTRL+C')
try:
    rmq_channel.start_consuming()
except KeyboardInterrupt:
    rmq_channel.stop_consuming()

#закрытие соединения
rmq_connection.close()
