import allure
import pytest
from library.common.db import *


@allure.feature('Adapter API')
@allure.story('Запрос списка городов')
@allure.title('Запрос списка городов')
def test_ad_get_cities(AuthServiceAPI, AdapterServiceApi, CatalogsAPIHelper, CatalogsServiceAPI):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AdapterServiceApi.GetCities(client=AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response)

    with allure.step(f"Проверка что функция не вернула не активный город"):
        with allure.step(f"Поиск не активного города"):
            db = DBClient('catalog')
            notActiveCity = db.fetchOne("SELECT * FROM cities WHERE active='f' LIMIT 1")
            cities = CatalogsAPIHelper.Cities(response)
            item = next((item for item in cities if item["cityCode"] == notActiveCity["city_code"]), None)
        with allure.step(f"Проверка что города не было в списке"):
            assert item is None, f"Город найден {notActiveCity['name']}, но не должен"

    response = CatalogsServiceAPI.ListCity({'isBoxberry': 'true'})
    citiesCatalog = CatalogsAPIHelper.Cities(response)

    assert len(cities) == len(citiesCatalog), f"Количество городов в ответе Adapter не равно количеству из сервиса Catalog с filter.isBoxberry = true"


@allure.feature('Adapter API')
@allure.story('Запрос списка городов с проверкой всех параметров')
@allure.title('Запрос списка городов с проверкой всех параметров')
def test_ad_get_cities_with_fields(AuthServiceAPI, AdapterServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AdapterServiceApi.GetCities(client=AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response, True)


@allure.feature('Adapter API')
@allure.story('Проверка фильтра')
@allure.title('Проверка фильтра')
@pytest.mark.parametrize("filters", [
    ({'countryCode': 643}),
    ({'countryCode': 398}),
    ({'pickupPointRF': 'true'}),
    ({'pickupPointRF': 'false'}),
    ({'countryCode': 643, 'courierDelivery': 'true'}),
    ({'countryCode': 804, 'courierDelivery': 'false'}),
    ({'pickupPointRF': 'false'}),
    ({'pickupPointRF': 'true'}),
])
def test_ad_get_cities_set(AuthServiceAPI, AdapterServiceApi, CatalogsAPIHelper, CatalogsServiceAPI, filters):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AdapterServiceApi.GetCities(filters, AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка что все найденные города с верными свойствами"):
        for city in cities:
            with allure.step(f"Проверка {city['name']}"):
                #allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                for param in filters:
                    paramName = param
                    if param == 'fiasID':
                        paramName = 'fiasId'
                    value = filters[param]
                    if value == 'true':
                        value = True
                    if value == 'false':
                        value = False
                    with allure.step(f"Проверка наличия {paramName}"):
                        assert paramName in city
                    with allure.step(f"Проверка значения {paramName}"):
                        assert city[paramName] == value

    filters['isBoxberry'] = 'true'

    response = CatalogsServiceAPI.ListCity(filters)
    citiesCatalog = CatalogsAPIHelper.Cities(response)

    assert len(cities) == len(citiesCatalog), f"Количество городов в ответе Adapter не равно количеству из сервиса Catalog с filter.isBoxberry = true"


@allure.feature('Adapter API')
@allure.story('Параметры в returnedField и данных ответа [C_PICKUP_POINT_RF]')
@allure.title('Параметры в returnedField и данных ответа [C_PICKUP_POINT_RF]')
def test_ad_city_return_field_pickupPointRF(AuthServiceAPI, AdapterServiceApi):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AdapterServiceApi.GetCities(query='?responseFields=C_PICKUP_POINT_RF', client=AuthServiceAPI.lastToken)
    response_body = response.json()

    assert 'cities' in response_body, f"cities нет в ответе"
    assert 'pickupPointRF' in response_body['cities'][0], f"Не найден pickupPointRF {response_body['cities'][0]}"
    assert len(response_body['cities'][0]) == 1, f"В ответе не только pickupPointRF {response_body['cities'][0]}"


@allure.feature('Aggregator API')
@allure.story('Параметры в returnedField и данных ответа [все поля]')
@allure.title('Параметры в returnedField и данных ответа [все поля]')
def test_ad_city_return_field_C_NAME_ENG(AuthServiceAPI, AdapterServiceApi):
    to_check = {"cityCode","name","receptionLap","deliveryLap","reception","pickupPoint","courierDelivery","foreignReceptionReturns","terminal","kladr","countryCode","region","postamat","courierReception","russianPostDelivery","nameEng","pickupPointRF"}
    fields = '"CITY_CODE" "NAME" "RECEPTION_LAP" "DELIVERY_LAP" "RECEPTION" "PICKUP_POINT" "COURIER_DELIVERY" "FOREIGN_RECEPTION_RETURNS" "TERMINAL" "KLADR" "COUNTRY_CODE" "REGION" "SHORT" "LOCATION" "INDEX" "PREFIX" "REGION_PREFIX" "LOCATION_PREFIX" "POSTOMAT" "COURIER_RECEPTION" "RUSSIAN_POST_DELIVERY" "POST_CODE" "TYPE_TRANSPORT" "FIAS_ID" "C_NAME_ENG" "C_PICKUP_POINT_RF"'
    fields = fields.split(' ')
    query = ''
    for f in fields:
        query = query + '&responseFields=' + f.strip('"')

    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AdapterServiceApi.GetCities(query='?countryCode=398' + query, client=AuthServiceAPI.lastToken)
    response_body = response.json()

    assert 'cities' in response_body, f"cities нет в ответе"

    for field in to_check:
        assert field in response_body['cities'][0], f"{field} отсутствует в city"
