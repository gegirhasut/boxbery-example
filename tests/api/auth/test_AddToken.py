import allure


@allure.feature('Auth API')
@allure.story('Создание существующего токена')
@allure.title('Создание существующего токена')
def test_token_add_existent(AuthServiceAPI, AuthAPIHelper):
    AuthServiceAPI.AddToken("ta-exist-token", "ta-exists-client-code")
    response = AuthServiceAPI.AddToken("ta-exist-token", "ta-exists-client-code")
    AuthAPIHelper.CheckResonse500HasUserWithTokenExists(response)


@allure.feature('Auth API')
@allure.story('Создание нового токена')
@allure.title('Создание нового токена')
def test_token_add_new(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()
    response = AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)
    AuthAPIHelper.CheckResponse200EmptyJson(response)
