import allure


@allure.feature('Auth API')
@allure.story('Создание токена, получение клиентского кода')
@allure.title('Создание токена, получение клиентского кода')
def test_get_access_valid_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()
    AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)
    response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)


@allure.feature('Auth API')
@allure.story('Получение не существующего токена')
@allure.title('Получение не существующего токена')
def test_get_access_absence_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()
    response = AuthServiceAPI.GetAccessToken(token=random_str)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)


@allure.feature('Auth API')
@allure.story('Возможность создать 2 токена 1 клиенту')
@allure.title('Возможность создать 2 токена 1 клиенту')
def test_get_access_1_client_2_tokens(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str_token1 = string_helper.get_random_string()
    random_str_token2 = string_helper.get_random_string()
    random_str_client = string_helper.get_random_string()

    AuthServiceAPI.AddToken("ta-token-" + random_str_token1, "ta-client-" + random_str_client)
    response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    AuthServiceAPI.AddToken("ta-token-" + random_str_token2, "ta-client-" + random_str_client)
    response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)


@allure.feature('Auth API')
@allure.story('Невозможность создать 1 токен для 2-х клиентов')
@allure.title('Невозможность создать 1 токен для 2-х клиентов')
def test_get_access_2_clients_1_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str_token = string_helper.get_random_string()
    random_str_client1 = string_helper.get_random_string()
    random_str_client2 = string_helper.get_random_string()

    AuthServiceAPI.AddToken("ta-token-" + random_str_token, "ta-client-" + random_str_client1)
    response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    response = AuthServiceAPI.AddToken("ta-token-" + random_str_token, "ta-client-" + random_str_client2)
    AuthAPIHelper.CheckResonse500HasUserWithTokenExists(response)


@allure.feature('Auth API')
@allure.story('Проверка получения прав к ресурсу')
@allure.title('Проверка получения прав к ресурсу')
def test_get_access_token_to_resource(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()
    resource = '/api.aggregator.Partner/AddToken'

    with allure.step('Создать токен и проверить, что доступа к ресурсу нет'):
        AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Дать доступ к ресурсу и проверить, что доступ появился'):
        AuthAPIHelper.AddTokenToRole("ta-token-" + random_str, "master")
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Отключить токен, доступ к ресурсу пропал'):
        AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Включить токен, доступ к ресурсу вернулся'):
        AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, True)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Заблокировать клиента, доступ к ресурсу пропал'):
        AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Разблокировать клиента, доступ к ресурсу вернулся'):
        AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Отобрать доступ к ресурсу, проверить что он недоступен'):
        AuthAPIHelper.RemoveTokenToRole("ta-token-" + random_str, "master")
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, resource)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)
