import allure


@allure.feature('Auth API')
@allure.story('Отключение и включение валидного токена')
@allure.title('Отключение и включение валидного токена')
def test_toggle_valid_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()

    with allure.step('Создать токен и проверить, что доступ есть'):
        AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Отключить токен, проверить что доступа нет'):
        response = AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Отключить токен еще раз, проверить что доступа нет'):
        response = AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Включить токен, проверить что доступ есть'):
        response = AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, True)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Включить токен еще раз, проверить что доступ есть'):
        response = AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, True)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)


@allure.feature('Auth API')
@allure.story('Отключение и включение не существующего токена')
@allure.title('Отключение и включение не существующего токена')
def test_toggle_absence_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()

    with allure.step('Проверка включения'):
        response = AuthServiceAPI.ToggleToken(random_str, True)
        AuthAPIHelper.CheckResponse404HasNoTokensAffected(response)
        response = AuthServiceAPI.GetAccessToken(random_str)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Проверка выключения'):
        response = AuthServiceAPI.ToggleToken(random_str, False)
        AuthAPIHelper.CheckResponse404HasNoTokensAffected(response)
        response = AuthServiceAPI.GetAccessToken(random_str)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)


@allure.feature('Auth API')
@allure.story('Отключение и включение токена клиенту с 2-мя токенами')
@allure.title('Отключение и включение токена клиенту с 2-мя токенами')
def test_toggle_token_client_with_2_tokens(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str_token1 = string_helper.get_random_string()
    random_str_token2 = string_helper.get_random_string()
    random_str_client = string_helper.get_random_string()

    with allure.step('Создание 2-х токенов для клиента'):
        AuthServiceAPI.AddToken("ta-token-" + random_str_token1, "ta-client-" + random_str_client)
        AuthServiceAPI.AddToken("ta-token-" + random_str_token2, "ta-client-" + random_str_client)

    with allure.step('Выключить 1 токен и проверить, что у токена 1 нет доступа, а у 2 есть'):
        response = AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)
        response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token1)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)
