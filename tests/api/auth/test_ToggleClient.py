import allure


@allure.feature('Auth API')
@allure.story('Отключение и включение клиента')
@allure.title('Отключение и включение клиента')
def test_toggle_valid_client(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()

    AuthServiceAPI.AddToken("ta-token-" + random_str, "ta-client-" + random_str)

    with allure.step('Заблокировать клиента, проверить что доступа нет доступ'):
        response = AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Заблокировать еще раз, проверить что доступа нет'):
        response = AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    with allure.step('Разблокировать клиента, проверить что доступ есть'):
        response = AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)

    with allure.step('Разблокировать клиента еще раз, проверить что доступ есть'):
        response = AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)
        AuthAPIHelper.CheckResponse200EmptyJson(response)
        response = AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)
        AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)


@allure.feature('Auth API')
@allure.story('Отключение и включение не существующего клиента')
@allure.title('Отключение и включение не существующего клиента')
def test_toggle_absence_client(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str = string_helper.get_random_string()

    response = AuthServiceAPI.ToggleClient(random_str, False)
    AuthAPIHelper.CheckResponse404HasNoTokensAffected(response)
    response = AuthServiceAPI.GetAccessToken(random_str)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    response = AuthServiceAPI.ToggleClient(random_str, True)
    AuthAPIHelper.CheckResponse404HasNoTokensAffected(response)
    response = AuthServiceAPI.GetAccessToken(random_str)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)


@allure.feature('Auth API')
@allure.story('1 клиент, токен 1 включен, токен 2 выключен')
@allure.title('1 клиент, токен 1 включен, токен 2 выключен')
def test_toggle_client_with_off_token(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str_token1 = string_helper.get_random_string()
    random_str_token2 = string_helper.get_random_string()
    random_str_client = string_helper.get_random_string()

    AuthServiceAPI.AddToken("ta-token-" + random_str_token1, "ta-client-" + random_str_client)
    AuthServiceAPI.AddToken("ta-token-" + random_str_token2, "ta-client-" + random_str_client)

    AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
    AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)

    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token1)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)
    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token2)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)

    AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)
    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token1)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)
    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token2)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)


@allure.feature('Auth API')
@allure.story('Смена статусов токенов в момент выключенного клиента')
@allure.title('Смена статусов токенов в момент выключенного клиента')
def test_toggle_tokens_while_client_is_off(AuthServiceAPI, AuthAPIHelper, string_helper):
    random_str_token1 = string_helper.get_random_string()
    random_str_token2 = string_helper.get_random_string()
    random_str_client = string_helper.get_random_string()

    AuthServiceAPI.AddToken("ta-token-" + random_str_token1, "ta-client-" + random_str_client)
    AuthServiceAPI.AddToken("ta-token-" + random_str_token2, "ta-client-" + random_str_client)

    AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, False)
    AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, True)
    AuthServiceAPI.ToggleToken("ta-token-" + random_str_token1, False)
    AuthServiceAPI.ToggleToken(AuthServiceAPI.lastToken, True)
    AuthServiceAPI.ToggleClient(AuthServiceAPI.lastClientCode, False)

    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token1)
    AuthAPIHelper.CheckResonse403HasPermissionDenied(response)
    response = AuthServiceAPI.GetAccessToken("ta-token-" + random_str_token2)
    AuthAPIHelper.CheckResponse200HasClientCode(response, AuthServiceAPI.lastClientCode)
