import allure
import pytest
from library.common.db import *


@allure.feature('Aggregator API')
@allure.story('Запрос списка стран и проверка свойств')
@allure.title('Запрос списка стран и проверка свойств')
def test_full_list(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCountries(response)


@allure.feature('Aggregator API')
@allure.story('Запрос списка стран с фильтром accessDeliveryPoints')
@allure.title('Запрос списка стран с фильтром accessDeliveryPoints')
@pytest.mark.parametrize("accessDeliveryPoints", [True, False])
def test_filter_accessDeliveryPoints(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper, accessDeliveryPoints):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters={
        'accessDeliveryPoints': 'true' if accessDeliveryPoints else 'false'})

    CatalogsAPIHelper.CheckResponse200HasCountries(response)
    countries = CatalogsAPIHelper.Countries(response)
    with allure.step(f"Проверка что все страны с accessDeliveryPoints = {accessDeliveryPoints}"):
        for country in countries:
            if accessDeliveryPoints:
                assert country['accessDeliveryPoints'] is True
            else:
                assert 'accessDeliveryPoints' not in country


@allure.feature('Aggregator API')
@allure.story('Запрос списка стран с фильтром accessDeliveryAddress')
@allure.title('Запрос списка стран с фильтром accessDeliveryAddress')
@pytest.mark.parametrize("accessDeliveryAddress", [True, False])
def test_filter_accessDeliveryAddress(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper, accessDeliveryAddress):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters=
    {'accessDeliveryAddress': 'true' if accessDeliveryAddress else 'false'})

    CatalogsAPIHelper.CheckResponse200HasCountries(response)
    countries = CatalogsAPIHelper.Countries(response)
    with allure.step(f"Проверка что все страны с accessDeliveryAddress = {accessDeliveryAddress}"):
        for country in countries:
            with allure.step(f"Проверка {country['country']}"):
                if accessDeliveryAddress:
                    assert country['accessDeliveryAddress'] is True
                else:
                    assert 'accessDeliveryAddress' not in country


@allure.feature('Aggregator API')
@allure.story('Запрос списка стран с фильтрами accessDeliveryPoints, accessDeliveryAddress')
@allure.title('Запрос списка стран с фильтрами accessDeliveryPoints, accessDeliveryAddress')
@pytest.mark.parametrize("accessDeliveryPoints,accessDeliveryAddress", [(False, True), (True, True), (False, False)])
def test_filter_accessDelivery(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper, accessDeliveryPoints,
                               accessDeliveryAddress):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters=
    {
        'accessDeliveryAddress': 'true' if accessDeliveryAddress else 'false',
        'accessDeliveryPoints': 'true' if accessDeliveryPoints else 'false',
    })

    CatalogsAPIHelper.CheckResponse200HasCountries(response)
    countries = CatalogsAPIHelper.Countries(response)
    with allure.step(
            f"Проверка что все страны с accessDeliveryAddress = {accessDeliveryAddress}, accessDeliveryPoints = {accessDeliveryPoints}"):
        for country in countries:
            if accessDeliveryAddress:
                assert country['accessDeliveryAddress'] is True
            else:
                assert 'accessDeliveryAddress' not in country
            if accessDeliveryPoints:
                assert country['accessDeliveryPoints'] is True
            else:
                assert 'accessDeliveryPoints' not in country


@allure.feature('Aggregator API')
@allure.story('Запрос пустого списка стран с фильтрами accessDeliveryAddress = false, accessDeliveryPoints = true')
@allure.title('Запрос пустого списка стран с фильтрами accessDeliveryAddress = false, accessDeliveryPoints = true')
def test_filter_accessDelivery_empty_list(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    with allure.step(f"Обновить данные по Эквадор для попадания в запрос"):
        db = DBClient('catalog')
        db.update(f"UPDATE countries "
                  f"SET access_delivery_points='f',"
                  f"access_delivery_address='t' where code='218'")

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters=
    {
        'accessDeliveryAddress': 'false',
        'accessDeliveryPoints': 'true',
    })

    CatalogsAPIHelper.CheckResponse200EmptyJson(response)


@allure.feature('Aggregator API')
@allure.story('Запрос страны по countryCode 840 (СОЕДИНЕННЫЕ ШТАТЫ)')
@allure.title('Запрос страны по countryCode 840 (СОЕДИНЕННЫЕ ШТАТЫ)')
def test_country_code_840_usa(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    with allure.step(f"Обновить данные по США в базе"):
        db = DBClient('catalog')
        db.update(f"UPDATE countries "
                  f"SET full_length_phone_number='12', telephone_code='+1', access_delivery_points='t',"
                  f"access_delivery_address='t' where code='840'")

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters={'countryCode': '840'})

    CatalogsAPIHelper.CheckResponse200WithCountry(response, {
        "countryCode": 840,
        "country": "СОЕДИНЕННЫЕ ШТАТЫ",
        "countryFullName": "Соединенные Штаты Америки",
        "accessDeliveryPoints": True,
        "accessDeliveryAddress": True,
        "telephoneCode": "+1",
        "fullLengthPhoneNumber": 12
    })


@allure.feature('Aggregator API')
@allure.story('Запрос страны по countryCode 398 (Казахстан)')
@allure.title('Запрос страны по countryCode 398 (Казахстан)')
def test_country_code_398_kz(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    db = DBClient('catalog')
    db.update(f"UPDATE countries "
              f"SET full_length_phone_number='9', telephone_code='+12', access_delivery_points='t',"
              f"access_delivery_address='t' where code='398'")

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters={'countryCode': '398'})

    CatalogsAPIHelper.CheckResponse200WithCountry(response, {
        "countryCode": 398,
        "country": "Казахстан",
        "countryFullName": "Республика Казахстан",
        "countryNameEng": "Respublika Kazahstan",
        "accessDeliveryPoints": True,
        "accessDeliveryAddress": True,
        "telephoneCode": "+12",
        "fullLengthPhoneNumber": 9
    })


@allure.feature('Aggregator API')
@allure.story('Запрос страны по countryCode 156 (Китай)')
@allure.title('Запрос страны по countryCode 156 (Китай)')
def test_all_filters_156_china(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    with allure.step(f"Обновить данные по Китаю в базе"):
        db = DBClient('catalog')
        db.update(f"UPDATE countries "
                  f"SET access_delivery_points='f',"
                  f"access_delivery_address='t' where code='156'")

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken, filters=
        {'countryCode': '156', 'accessDeliveryAddress': 'true', 'accessDeliveryPoints': 'false'}
                                                 )

    CatalogsAPIHelper.CheckResponse200WithCountry(response, {
        "countryCode": 156,
        "country": "КИТАЙ",
        "countryFullName": "Китайская Народная Республика",
        "accessDeliveryAddress": True,
    })


@allure.feature('Aggregator API')
@allure.story('Запрос списка стран с ошибкой в фильтре')
@allure.title('Запрос списка стран с ошибкой в фильтре')
def test_filter_bad_value(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-countries', 'client-get-countries')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(client=AuthServiceAPI.lastToken,
                                                 filters={'accessDeliveryPoints': True})
    CatalogsAPIHelper.CheckResponse400HasBadBoolValue(response)


@allure.feature('Aggregator API')
@allure.story('#8 параметры в returnedField и данных ответа [C_NAME_ENG]')
@allure.title('#8 параметры в returnedField и данных ответа [C_NAME_ENG]')
def test_city_return_field_C_NAME_ENG(AuthServiceAPI, AggregatorServiceApi, array_helper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCountries(query='?countryCode=156&returnedFields=CNTR_COUNTRY_CODE&returnedFields=CNTR_COUNTRY_FULL_NAME', client=AuthServiceAPI.lastToken)

    array_helper.assert_arrays(response.json(), {
        "countries": [{"countryCode": 156,
            "countryFullName": "Китайская Народная Республика"}]
    })