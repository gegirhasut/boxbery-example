import allure
import pytest
from library.common.db import *


@allure.feature('Aggregator API')
@allure.story('Запрос списка городов')
@allure.title('Запрос списка городов')
def test_ag_get_cities(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(client=AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response)

    with allure.step(f"Проверка что функция не вернула не активный город"):
        with allure.step(f"Поиск не активного города"):
            db = DBClient('catalog')
            notActiveCity = db.fetchOne("SELECT * FROM cities WHERE active='f' LIMIT 1")
            cities = CatalogsAPIHelper.Cities(response)
            item = next((item for item in cities if item["cityCode"] == notActiveCity["city_code"]), None)
        with allure.step(f"Проверка что города не было в списке"):
            assert item is None, f"Город найден {notActiveCity['name']}, но не должен"


@allure.feature('Aggregator API')
@allure.story('Запрос списка городов с проверкой всех параметров')
@allure.title('Запрос списка городов с проверкой всех параметров')
def test_ag_get_cities_with_fields(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(client=AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response, True)


@allure.feature('Aggregator API')
@allure.story('Проверка фильтра')
@allure.title('Проверка фильтра')
@pytest.mark.parametrize("filters", [
    ({'countryCode': 804, 'pickupPoint': 'true'}),
    ({'countryCode': 804, 'pickupPoint': 'false'}),
    ({'countryCode': 643, 'reception': 'true'}),
    ({'countryCode': 643, 'reception': 'false'}),
    ({'countryCode': 804, 'deliveryLap': 'true'}),
    ({'countryCode': 804, 'deliveryLap': 'false'}),
    ({'countryCode': 643, 'receptionLap': 'true'}),
    ({'countryCode': 804, 'receptionLap': 'false'}),
    ({'countryCode': 643, 'courierDelivery': 'true'}),
    ({'countryCode': 804, 'courierDelivery': 'false'}),
    ({'countryCode': 804, 'courierReception': 'true'}),
    ({'countryCode': 804, 'courierReception': 'false'}),
    ({'countryCode': 643, 'fiasID': '862622ef-3340-4137-bbcb-7cbf4fe8cc94'}),
    ({'pickupPointRF': 'false'}),
    ({'pickupPointRF': 'true'}),
    ({'cityCode': 'Н00168975'}),
    ({'cityCode': 'Н00168975', 'pickupPoint': 'false', 'reception': 'false', 'deliveryLap': 'false',
      'receptionLap': 'false', 'courierDelivery': 'false', 'courierReception': 'false'}),
])
def test_ag_get_cities_set(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper, filters):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(filters, AuthServiceAPI.lastToken)
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка что все найденные города с верными свойствами"):
        for city in cities:
            with allure.step(f"Проверка {city['name']}"):
                #allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                for param in filters:
                    paramName = param
                    if param == 'fiasID':
                        paramName = 'fiasId'
                    value = filters[param]
                    if value == 'true':
                        value = True
                    if value == 'false':
                        value = False
                    with allure.step(f"Проверка наличия {paramName}"):
                        assert paramName in city
                    with allure.step(f"Проверка значения {paramName}"):
                        assert city[paramName] == value


@allure.feature('Aggregator API')
@allure.story('Запрос города (Н00165202, Авангард) и проверка всех его свойств')
@allure.title('Запрос города (Н00165202, Авангард) и проверка всех его свойств')
def test_ag_city_all_fields(AuthServiceAPI, AggregatorServiceApi, CatalogsAPIHelper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities({'cityCode': 'Н00165202'}, AuthServiceAPI.lastToken)

    CatalogsAPIHelper.CheckResponse200WithCity(response, {
         "cityCode": "Н00165202",
         "name": "Авангард",
         "receptionLap": False,
         "deliveryLap": False,
         "reception": False,
         "pickupPoint": False,
         "courierDelivery": False,
         "countryCode": 804,
         "courierReception": False,
      })


@allure.feature('Aggregator API')
@allure.story('#6 фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#6 фильтр список кодов города Boxberry - filter.cityCodes')
def test_ag_city_filter_cityCodes(AuthServiceAPI, AggregatorServiceApi, array_helper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(query='?cityCodes=Н00168975&cityCodes=02326', client=AuthServiceAPI.lastToken)

    array_helper.assert_arrays(response.json(), {
        "cities": [
            {
                "cityCode": "02326",
                "name": "Юрьев-Польский",
                "receptionLap": False,
                "deliveryLap": False,
                "reception": False,
                "pickupPoint": False,
                "courierDelivery": True,
                "foreignReceptionReturns": False,
                "terminal": False,
                "kladr": "3301700100000",
                "countryCode": 643,
                "region": "Владимирская",
                "prefix": "г",
                "postamat": False,
                "courierReception": False,
                "russianPostDelivery": False,
                "fiasId": "4859c31b-fba4-460a-9543-befc791da8a9",
                "pickupPointRF": False
            },
            {
                "cityCode": "Н00168975",
                "name": "Абазовка",
                "receptionLap": False,
                "deliveryLap": False,
                "reception": False,
                "pickupPoint": False,
                "courierDelivery": False,
                "foreignReceptionReturns": False,
                "terminal": False,
                "kladr": "",
                "countryCode": 804,
                "region": "Полтавская область",
                "postamat": False,
                "courierReception": False,
                "russianPostDelivery": False,
                "pickupPointRF": False
            }
        ]
    })


@allure.feature('Aggregator API')
@allure.story('#7 параметры в returnedField и данных ответа [C_PICKUP_POINT_RF]')
@allure.title('#7 параметры в returnedField и данных ответа [C_PICKUP_POINT_RF]')
def test_ag_city_return_field_pickupPointRF(AuthServiceAPI, AggregatorServiceApi):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(query='?returnedFields=C_PICKUP_POINT_RF', client=AuthServiceAPI.lastToken)
    response_body = response.json()

    assert 'cities' in response_body, f"cities нет в ответе"
    assert 'pickupPointRF' in response_body['cities'][0], f"Не найден pickupPointRF {response_body['cities'][0]}"
    assert len(response_body['cities'][0]) == 1, f"В ответе не только pickupPointRF {response_body['cities'][0]}"


@allure.feature('Aggregator API')
@allure.story('#8 параметры в returnedField и данных ответа [C_NAME_ENG]')
@allure.title('#8 параметры в returnedField и данных ответа [C_NAME_ENG]')
def test_city_return_field_C_NAME_ENG(AuthServiceAPI, AggregatorServiceApi, array_helper):
    AuthServiceAPI.AddToken('token-get-cities', 'client-get-cities')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetCities(query='?cityCode=150&returnedFields=C_NAME_ENG&returnedFields=C_CITY_CODE', client=AuthServiceAPI.lastToken)

    array_helper.assert_arrays(response.json(), {
        "cities": [{"cityCode": "150",
            "nameEng": "Russia"}]
    })
