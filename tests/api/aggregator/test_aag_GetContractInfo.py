import allure
import pytest
from library.common.db import *


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "code"')
@allure.title('Передан параметр поиска "code"')
def test_get_contract_info(AuthServiceAPI, AggregatorServiceApi, PartnerAPIHelper):
    AuthServiceAPI.AddToken('token-get-contract-info', 'token-get-contract-info')
    AuthServiceAPI.GetAccessToken(AuthServiceAPI.lastToken, AuthServiceAPI.lastToken)

    response = AggregatorServiceApi.GetContractInfo(filters={'partnerCode': f"BBR0006083"}, client=AuthServiceAPI.lastToken)
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 200, {
        "shops":
            [{"code": "BBR5001670", "name": "ИП Кузнецова Елена Николаевна"}]
    })