import allure
import pytest


@allure.feature('Partner API')
@allure.story('Проверки подключения услуги у ИМ')
@allure.title('Проверки подключения услуги у ИМ')
def test_check_service_shop(PartnerServiceAPI, PartnerAPIHelper):
    with allure.step('Запрос существующей услуги'):
        response = PartnerServiceAPI.CheckServiceShop(
            {
                'partnerCode': f"BBR0012112",
                'serviceCode': '000001417'
            }
        )

        PartnerAPIHelper.CheckResponse200NotEmptyJson(response)
        assert response.json() == {"result": True}, f"В ответе ожидается result: True"

    with allure.step('Запрос не существующей услуги'):
        response = PartnerServiceAPI.CheckServiceShop(
            {
                'partnerCode': f"BBR0012112",
                'serviceCode': '000001385'
            }
        )

        PartnerAPIHelper.CheckResponse200EmptyJson(response)


@allure.feature('Partner API')
@allure.story('Проверки подключения услуги - негативные кейсы')
@allure.title('Проверки подключения услуги - негативные кейсы')
def test_check_service_shop_negative(PartnerServiceAPI, PartnerAPIHelper):
    with allure.step('Запрос без serviceCode'):
        response = PartnerServiceAPI.CheckServiceShop(
            {
                'partnerCode': f"BBR0012112"
            }
        )

        PartnerAPIHelper.CheckResponseCodeAndJson(response, 500,
                                                  {
                                                      "error": "invalid field ServiceCode: message must exist",
                                                      "code": 2,
                                                      "message": "invalid field ServiceCode: message must exist"
                                                  })

    with allure.step('Запрос без partnerCode'):
        response = PartnerServiceAPI.CheckServiceShop(
            {
                'serviceCode': '000001385'
            }
        )

        PartnerAPIHelper.CheckResponseCodeAndJson(response, 500,
                                                  {
                                                      "error": "invalid field PartnerCode: message must exist",
                                                      "code": 2,
                                                      "message": "invalid field PartnerCode: message must exist"
                                                  })

    with allure.step('Запрос без параметров'):
        response = PartnerServiceAPI.CheckServiceShop()

        PartnerAPIHelper.CheckResponseCodeAndJson(response, 500,
                                                  {
                                                      "error": "invalid field PartnerCode: message must exist",
                                                      "code": 2,
                                                      "message": "invalid field PartnerCode: message must exist"
                                                  })
