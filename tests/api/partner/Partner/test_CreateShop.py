import allure
import pytest


@allure.feature('Partner API')
@allure.story('Создание потребителя')
@allure.title('Создание потребителя')
@pytest.mark.parametrize("clientType, settings", [
    (None, {
        "clientName": "",
        "filtersPoint":
            [{"id": "1", "values": [0], "isAnd": False, "groupValues": [1, 3]},
             {"id": "2", "isAnd": False},
             {"id": "3", "isAnd": False},
             {"id": "4", "isAnd": False},
             {"id": "5", "isAnd": False},
             {"id": "6", "values": [1, 2], "isAnd": False},
             {"id": "7", "values": [643], "isAnd": False},
             {"id": "8", "values": [2], "isAnd": False},
             {"id": "9", "isAnd": True},
             ]
    }),
    ("LK_IM", {
        "clientName": "",
        "filtersPoint":
            [{"id": "1", "values": [0], "isAnd": False, "groupValues": [1, 3]},
             {"id": "2", "isAnd": False},
             {"id": "3", "isAnd": False},
             {"id": "4", "isAnd": False},
             {"id": "5", "isAnd": False},
             {"id": "6", "values": [1, 2], "isAnd": False},
             {"id": "7", "values": [643], "isAnd": False},
             {"id": "8", "values": [2], "isAnd": False},
             {"id": "9", "isAnd": True},
             ]
    }),
    ("TRADING_PLATFORM", {
        "clientName": "",
        "filtersPoint":
            [{"id": "1", "values": [0], "isAnd": False, "groupValues": [1, 3]},
             {"id": "2", "isAnd": False},
             {"id": "3", "isAnd": False},
             {"id": "4", "isAnd": False},
             {"id": "5", "isAnd": False},
             {"id": "6", "isAnd": False},
             {"id": "7", "values": [643], "isAnd": False},
             {"id": "8", "values": [2], "isAnd": False},
             {"id": "9", "isAnd": True}
             ]
    }),
    ("LK_SPVZ", {
        "clientName": "",
        "filtersPoint":
            [{"id": "1", "values": [0], "isAnd": False, "groupValues": [1, 3]},
             {"id": "2", "isAnd": False},
             {"id": "3", "isAnd": False},
             {"id": "4", "isAnd": False},
             {"id": "5", "isAnd": False},
             {"id": "6", "isAnd": False},
             {"id": "7", "values": [643], "isAnd": False},
             {"id": "8", "values": [2], "isAnd": False},
             {"id": "9", "isAnd": True}
             ]
    }),
    ("SITE", {
        "clientName": "",
        "filtersPoint":
            [{"id": "1", "values": [0], "isAnd": False, "groupValues": [1, 3]},
             {"id": "2", "isAnd": False},
             {"id": "3", "isAnd": False},
             {"id": "4", "isAnd": False},
             {"id": "5", "isAnd": False},
             {"id": "6", "isAnd": False},
             {"id": "7", "values": [643], "isAnd": False},
             {"id": "8", "values": [2], "isAnd": False},
             {"id": "9", "isAnd": True}
             ]
    })
])
def test_create_shop(PartnerServiceAPI, PartnerAPIHelper, string_helper, clientType, settings):
    shop_postfix = string_helper.get_random_string(10)

    response = PartnerServiceAPI.CreateShop(
        {
            'clientCode': f"at-test-shop-{shop_postfix}", 'name': f"AT: Test Shop Create ({shop_postfix})",
            'clientType': clientType
        }
    )
    PartnerAPIHelper.CheckResponse200Created(response)

    with allure.step('Проверка ShopSettings'):
        response = PartnerServiceAPI.GetShopSettings(f"at-test-shop-{shop_postfix}")
        response = response.json()
        settings['clientName'] = f"AT: Test Shop Create ({shop_postfix})"
        #allure.attach(str(response), 'JSON SETTINGS', allure.attachment_type.JSON)
        with allure.step('Проверка наличия clientName'):
            assert "clientName" in response, f"Отсутствует clientName. Свойства: " + str(settings) + ". Ответ: " + str(response)
        with allure.step(f"Проверка clientName = {settings['clientName']}"):
            assert response["clientName"] == settings['clientName'], f"Не верный clientName. Свойства: " + str(settings) + ". Ответ: " + str(response)
        with allure.step('Проверка наличия filtersPoint'):
            assert "filtersPoint" in response, f"Отсутствует filtersPoint. Свойства: " + str(settings) + ". Ответ: " + str(response)
        with allure.step('Проверка значения filtersPoint'):
            sorted_response = sorted(response["filtersPoint"], key=lambda item: item['id'])
            assert sorted_response == settings['filtersPoint'], f"Не верные filtersPoint. Свойства: " + str(settings) + ". Ответ: " + str(response)


@allure.feature('Partner API')
@allure.story('Создание существующего потребителя не возможно')
@allure.title('Создание существующего потребителя не возможно')
def test_create_shop_exists(PartnerServiceAPI, PartnerAPIHelper, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    with allure.step('Создание потребителя'):
        response = PartnerServiceAPI.CreateShop(
            {
                'clientCode': f"at-test-shop-{shop_postfix}", 'name': f"AT: Test Shop Create ({shop_postfix})",
                'clientType': None
            }
        )
    with allure.step('Проверка успешного создания'):
        PartnerAPIHelper.CheckResponse200Created(response)

    with allure.step('Создание существующего потребителя'):
        response = PartnerServiceAPI.CreateShop(
            {
                'clientCode': f"at-test-shop-{shop_postfix}", 'name': f"AT: Test Shop Create ({shop_postfix})",
                'clientType': None
            }
        )
    with allure.step('Проверка на 500 при создании, контрагент существует'):
        PartnerAPIHelper.CheckResponse500Exists(response)

    with allure.step('Создание существующего потребителя другого типа'):
        response = PartnerServiceAPI.CreateShop(
            {
                'clientCode': f"at-test-shop-{shop_postfix}", 'name': f"AT: Test Shop Create ({shop_postfix})",
                'clientType': 'TRADING_PLATFORM'
            }
        )
    with allure.step('Проверка на 500 при создании, контрагент существует'):
        PartnerAPIHelper.CheckResponse500Exists(response)
