#https://confluence.boxberry.ru/pages/viewpage.action?pageId=40868578 cases (at the bottom!)
from library.common.db import *
import allure
from datetime import date
import datetime
import pytest


@allure.feature('Partner API')
@allure.story('#1,2 Проверка фильтров по умолчанию для clientType = 1,2')
@allure.title('#1,2 Проверка фильтров по умолчанию для clientType = 1,2')
@pytest.mark.parametrize("clientType", ['LK_IM', 'TRADING_PLATFORM'])
def test_set_shop_settings1_2(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, clientType):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-list-pick-point-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test List Pick Point ({shop_postfix})",
            'clientType': clientType
        }
    )

    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode, 'page.limit': 20, 'page.offset': 0})

        with allure.step('Проверка'):
            CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

            with allure.step('Проверка points в ответе'):
                response_json = response.json()
                with allure.step('Наличие points в ответе'):
                    assert 'points' in response_json, f"Нет points в ответе"
                with allure.step('Количество points > 0'):
                    assert len(response_json['points']) > 0, f"Пустой points в ответе"

            with allure.step('Проверка признаков'):
                for point in response_json['points']:
                    assert 'points' in response_json, f"Не найден массив points в ответе"
                    with allure.step(f"Проверка {point['name']}"):
                        with allure.step(f"foreignOnlineStoresOnly == false"):
                            assert point['foreignOnlineStoresOnly'] is False, \
                                f"Условие не выполнилось " + str(point)
                        with allure.step(f"partnerСode = 0 и (PickupPoint = true или Terminal = true)"):
                            assert point['partnerCode'] == 0 and (point['pickupPoint'] is True or point['terminal'] is True), \
                                f"Условие не выполнилось " + str(point)
                        with allure.step(f"reception = true или issuanceBoxberry = true"):
                            assert point['reception'] is True or point['issuanceBoxberry'] is True, \
                                f"Условие не выполнилось " + str(point)
                        with allure.step(f"countryCode = 643"):
                            assert point['countryCode'] == 643, \
                                f"Условие не выполнилось " + str(point)
                        with allure.step(f"countryCode = 643"):
                            assert point['status'] == 2, \
                                f"Условие не выполнилось " + str(point)


@allure.feature('Partner API')
@allure.story('#4 Проверка фильтра partnerType - с фильтрацией по ЛК Boxberry  + без фильтрации по значениям группы')
@allure.title('#4 Проверка фильтра partnerType - с фильтрацией по ЛК Boxberry  + без фильтрации по значениям группы')
def test_set_shop_settings4(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    partnerCode = 0
    filter = [{"id":"1","values":[{"value":0,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter)


@allure.feature('Partner API')
@allure.story('#5 Проверка фильтра partnerType - с фильтрацией по Beeline + без фильтрации по значениям группы')
@allure.title('#5 Проверка фильтра partnerType - с фильтрацией по Beeline + без фильтрации по значениям группы')
def test_set_shop_settings5(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    partnerCode = 1
    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter)


@allure.feature('Partner API')
@allure.story('#6 Проверка фильтра partnerType - с фильтрацией по PickPoint + без фильтрации по значениям группы')
@allure.title('#6 Проверка фильтра partnerType - с фильтрацией по PickPoint + без фильтрации по значениям группы')
def test_set_shop_settings6(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    partnerCode = 2
    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter)


@allure.feature('Partner API')
@allure.story('#7 Проверка фильтра partnerType - с фильтрацией по Qiwi + без фильтрации по значениям группы')
@allure.title('#7 Проверка фильтра partnerType - с фильтрацией по Qiwi + без фильтрации по значениям группы')
def test_set_shop_settings7(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    partnerCode = 3
    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter)


@allure.feature('Partner API')
@allure.story('#8 Проверка фильтра partnerType - с фильтрацией по Lamoda + без фильтрации по значениям группы')
@allure.title('#8 Проверка фильтра partnerType - с фильтрацией по Lamoda + без фильтрации по значениям группы')
def test_set_shop_settings8(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    partnerCode = 4
    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter)


def f_set_shop_settings_partner_code(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper, partnerCode, filter):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-list-test-settings-pc-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test List Pick Point ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    PartnerServiceAPI.SetShopSettings(imCode, filter)

    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode, 'page.limit': 20, 'page.offset': 0})
        response_json = response.json()

        with allure.step('Проверка'):
            CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
            with allure.step('Проверка признаков'):
                assert 'points' in response_json, f"Не найден массив points в ответе"
                for point in response_json['points']:
                    with allure.step(f"partnerСode = {partnerCode}?"):
                        assert point['partnerCode'] == partnerCode, \
                            f"Условие не выполнилось " + str(point)


@allure.feature('Partner API')
@allure.story('#9 Проверка фильтра partnerType - с фильтрацией по partnerType  + без фильтрации по значениям группы')
@allure.title('#9 Проверка фильтра partnerType - с фильтрацией по partnerType  + без фильтрации по значениям группы')
def test_set_shop_settings9(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-list-test-settings-all-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test List Pick Point ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка количества partnerCode'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            points = {0: 0, 1: 0, 2: 0, 3: 0}
            for point in response_json['points']:
                assert point['partnerCode'] in points, f"Найден лишний partnerCode {point['partnerCode']}"
                points[point['partnerCode']] = points[point['partnerCode']] + 1

            for point in points:
                assert points[point] > 0, f"Количество точек с partnerCode={point} должно быть > 0, найдено {points[point]}"


@allure.feature('Partner API')
@allure.story('#10 Проверка фильтра partnerType - с фильтрацией по всем partnerType + с фильтрацией по выборочным значениям группы')
@allure.title('#10 Проверка фильтра partnerType - с фильтрацией по всем partnerType + с фильтрацией по выборочным значениям группы')
def test_set_shop_settings10(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-list-test-settings-all2-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test List Pick Point ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False}]},{"value":1,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True}]},{"value":3,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":False}]},{"value":4,"enabled":True,"values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":True}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка количества partnerCode'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            points = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0}
            for point in response_json['points']:
                assert point['partnerCode'] in points, f"Найден лишний partnerCode {point['partnerCode']}"
                if point['partnerCode'] == 0:
                    assert point['postomat'] is True, f"Не выполняется условие partnerCode = 0 и Postomat = true " + str(point)
                elif point['partnerCode'] == 1:
                    assert point['pickupPoint'] is True, f"Не выполняется условие partnerCode= 1 и PickupPoint = true " + str(point)
                elif point['partnerCode'] == 2:
                    assert point['terminal'] is True, f"Не выполняется условие partnerCode= 2 и Terminal = true " + str(point)
                elif point['partnerCode'] == 3:
                    assert point['pickupPoint'] is True or point['postomat'] is True,\
                        f"Не выполняется условие partnerCode= 3 и (PickupPoint = true или Postomat = true) " + str(point)
                elif point['partnerCode'] == 4:
                    assert point['postomat'] is True or point['terminal'] is True, \
                        f"Не выполняется условие partnerCode= 4 и (Postomat = true или Terminal = true) " + str(point)

                points[point['partnerCode']] = points[point['partnerCode']] + 1

            for point in points:
                assert points[point] > 0, f"Количество точек с partnerCode={point} должно быть > 0, найдено {points[point]}"


@allure.feature('Partner API')
@allure.story('#11 Проверка фильтра partnerType - с фильтрацией по всем partnerType + с фильтрацией по всем значениям группы')
@allure.title('#11 Проверка фильтра partnerType - с фильтрацией по всем partnerType + с фильтрацией по всем значениям группы')
def test_set_shop_settings11(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-list-test-settings-all3-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings all3 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}]},{"value":1,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}]},{"value":2,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}]},{"value":3,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}]},{"value":4,"enabled":True,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка количества partnerCode'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            points = {0: 0, 1: 0, 2: 0, 3: 0, 4: 0}
            for point in response_json['points']:
                assert point['partnerCode'] in points, f"Найден лишний partnerCode {point['partnerCode']}"
                assert point['postomat'] is True or point['terminal'] is True or point['pickupPoint'] is True, \
                    f"Не выполняется условие Postomat = true или PickupPoint = true или Terminal = true " + str(point)

                points[point['partnerCode']] = points[point['partnerCode']] + 1

            for point in points:
                assert points[point] > 0, f"Количество точек с partnerCode={point} должно быть > 0, найдено {points[point]}"


@allure.feature('Partner API')
@allure.story('#12 Проверка фильтра issueType - отделения без вскрытия = все отделения')
@allure.title('#12 Проверка фильтра issueType - отделения без вскрытия = все отделения')
def test_set_shop_settings12(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с признаками'):
            checks = {'enableOpening': {True: 0, False: 0}, 'enablePartialDelivery': {True: 0, False: 0}}
            for point in response_json['points']:
                checks['enableOpening'][point['enableOpening']] = checks['enableOpening'][point['enableOpening']] + 1
                checks['enablePartialDelivery'][point['enablePartialDelivery']] = checks['enablePartialDelivery'][point['enablePartialDelivery']] + 1

            assert checks['enableOpening'][True] > 0, \
                f"Количество точек с enableOpening = true должно быть > 0, найдено {checks['enableOpening'][True]}"
            assert checks['enableOpening'][False] > 0, \
                f"Количество точек с enableOpening = false должно быть > 0, найдено {checks['enableOpening'][False]}"
            assert checks['enablePartialDelivery'][True] > 0, \
                f"Количество точек с enablePartialDelivery = true должно быть > 0, найдено {checks['enablePartialDelivery'][True]}"
            assert checks['enablePartialDelivery'][False] > 0, \
                f"Количество точек с enablePartialDelivery = false должно быть > 0, найдено {checks['enablePartialDelivery'][False]}"


@allure.feature('Partner API')
@allure.story('#13 Проверка фильтра issueType - отделения с возможностью вскрытия')
@allure.title('#13 Проверка фильтра issueType - отделения с возможностью вскрытия')
def test_set_shop_settings13(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-openning-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable openning ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с только EnableOpening = true'):
            for point in response_json['points']:
                assert point['enableOpening'] is True,\
                    f"В ответе должны быть только отделения с enableOpening = true " + str(point)


@allure.feature('Partner API')
@allure.story('#14 Проверка фильтра issueType - отделения с частичной выдачей посылок')
@allure.title('#14 Проверка фильтра issueType - отделения с частичной выдачей посылок')
def test_set_shop_settings14(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-partial-delivery-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable delivery ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с только enablePartialDelivery = true'):
            for point in response_json['points']:
                assert point['enablePartialDelivery'] is True,\
                    f"В ответе должны быть только отделения с enablePartialDelivery = true " + str(point)


@allure.feature('Partner API')
@allure.story('#15 Проверка фильтра issueType -отделения с возможностью вскрытия и с частичной выдачей посылок')
@allure.title('#15 Проверка фильтра issueType -отделения с возможностью вскрытия и с частичной выдачей посылок')
def test_set_shop_settings15(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-opening-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable opening ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с только enablePartialDelivery = true и enableOpening = true'):
            for point in response_json['points']:
                assert point['enablePartialDelivery'] is True and point['enableOpening'] is True,\
                    f"В ответе должны быть только отделения с enablePartialDelivery = true и enableOpening = true " + str(point)


@allure.feature('Partner API')
@allure.story('#16 Проверка фильтра issueType - отделения без вскрытия и с возможностью вскрытия и с частичной выдачей посылок')
@allure.title('#16 Проверка фильтра issueType - отделения без вскрытия и с возможностью вскрытия и с частичной выдачей посылок')
def test_set_shop_settings16(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-opening-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable opening ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с только enablePartialDelivery = true и enableOpening = true'):
            for point in response_json['points']:
                assert point['enablePartialDelivery'] is True and point['enableOpening'] is True,\
                    f"В ответе должны быть только отделения с enablePartialDelivery = true и enableOpening = true " + str(point)


@allure.feature('Partner API')
@allure.story('#17 Проверка фильтра issueType - отделения без вскрытия или с возможностью вскрытия или с частичной выдачей посылок')
@allure.title('#17 Проверка фильтра issueType - отделения без вскрытия или с возможностью вскрытия или с частичной выдачей посылок')
def test_set_shop_settings17(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-enable-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings enable ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":False},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с признаками'):
            checks = {'enableOpening': {True: 0, False: 0}, 'enablePartialDelivery': {True: 0, False: 0}}
            for point in response_json['points']:
                checks['enableOpening'][point['enableOpening']] = checks['enableOpening'][point['enableOpening']] + 1
                checks['enablePartialDelivery'][point['enablePartialDelivery']] = checks['enablePartialDelivery'][point['enablePartialDelivery']] + 1

            assert checks['enableOpening'][True] > 0, \
                f"Количество точек с enableOpening = true должно быть > 0, найдено {checks['enableOpening'][True]}"
            assert checks['enableOpening'][False] > 0, \
                f"Количество точек с enableOpening = false должно быть > 0, найдено {checks['enableOpening'][False]}"
            assert checks['enablePartialDelivery'][True] > 0, \
                f"Количество точек с enablePartialDelivery = true должно быть > 0, найдено {checks['enablePartialDelivery'][True]}"
            assert checks['enablePartialDelivery'][False] > 0, \
                f"Количество точек с enablePartialDelivery = false должно быть > 0, найдено {checks['enablePartialDelivery'][False]}"


@allure.feature('Partner API')
@allure.story('#18 Проверка фильтра typeTransport - фильтрация автотранспорт')
@allure.title('#18 Проверка фильтра typeTransport - фильтрация автотранспорт')
def test_set_shop_settings18(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-18-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 18 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с typeTransport = 1'):
            for point in response_json['points']:
                assert 1 in point['typeTransport'], \
                    f"В ответе должны быть отделения с typeTransport = 1 " + str(point)


@allure.feature('Partner API')
@allure.story('#19 Проверка фильтра typeTransport - фильтрация авиадоставка')
@allure.title('#19 Проверка фильтра typeTransport - фильтрация авиадоставка')
def test_set_shop_settings19(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-19-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 19 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с typeTransport = 2'):
            for point in response_json['points']:
                assert 2 in point['typeTransport'], \
                    f"В ответе должны быть отделения с typeTransport = 2 " + str(point)


@allure.feature('Partner API')
@allure.story('#20 Проверка фильтра typeTransport - фильтрация ж/д')
@allure.title('#20 Проверка фильтра typeTransport - фильтрация ж/д')
def test_set_shop_settings20(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-20-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 20 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с typeTransport = 3'):
            for point in response_json['points']:
                assert 3 in point['typeTransport'], \
                    f"В ответе должны быть отделения с typeTransport = 3 " + str(point)


@allure.feature('Partner API')
@allure.story('#21 Проверка фильтра typeTransport - фильтрация автотранспорт и авиадоставка и ж/д')
@allure.title('#21 Проверка фильтра typeTransport - фильтрация автотранспорт и авиадоставка и ж/д')
def test_set_shop_settings21(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-21-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 21 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            for point in response_json['points']:
                assert 1 in point['typeTransport'], \
                    f"В ответе нет typeTransport = 1 " + str(point)
                assert 2 in point['typeTransport'], \
                    f"В ответе нет typeTransport = 2 " + str(point)
                assert 3 in point['typeTransport'], \
                    f"В ответе нет typeTransport = 3 " + str(point)


@allure.feature('Partner API')
@allure.story('#22 Проверка фильтра typeTransport - фильтрация автотранспорт или авиадоставка или ж/д')
@allure.title('#22 Проверка фильтра typeTransport - фильтрация автотранспорт или авиадоставка или ж/д')
def test_set_shop_settings22(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-22-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 22 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с typeTransport = 1 или 2 или 3'):
            for point in response_json['points']:
                assert 1 in point['typeTransport'] or\
                       2 in point['typeTransport'] or \
                       3 in point['typeTransport'],\
                    f"В ответе должно быть typeTransport = 1 или 2 или 3 " + str(point)


@allure.feature('Partner API')
@allure.story('#23 Проверка fittingType - фильтрация нет примерки')
@allure.title('#23 Проверка fittingType - фильтрация нет примерки')
def test_set_shop_settings23(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-23-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 23 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":True},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с fitting = 0'):
            for point in response_json['points']:
                assert point['fitting'] == 0, \
                    f"В ответе должны быть отделения с fitting = 0 " + str(point)


@allure.feature('Partner API')
@allure.story('#24 Проверка fittingType - фильтрация Частичная примерка')
@allure.title('#24 Проверка fittingType - фильтрация Частичная примерка')
def test_set_shop_settings24(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-24-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 24 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":True},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с fitting = 1'):
            for point in response_json['points']:
                assert point['fitting'] == 1, \
                    f"В ответе должны быть отделения с fitting = 1 " + str(point)


@allure.feature('Partner API')
@allure.story('#25 Проверка fittingType - фильтрация Полная примерка')
@allure.title('#25 Проверка fittingType - фильтрация Полная примерка')
def test_set_shop_settings25(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-25-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 25 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":True}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений с fitting = 2'):
            for point in response_json['points']:
                assert point['fitting'] == 2, \
                    f"В ответе должны быть отделения с fitting = 2 " + str(point)


@allure.feature('Partner API')
@allure.story('#26 Проверка fittingType - фильтрация нет примерки или Частичная примерка или Полная примерка')
@allure.title('#26 Проверка fittingType - фильтрация нет примерки или Частичная примерка или Полная примерка')
def test_set_shop_settings26(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-26-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 26 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":True},{"value":1,"enabled":True},{"value":2,"enabled":True}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка количества fitting'):
            points = {0: 0, 1: 0, 2: 0}
            for point in response_json['points']:
                points[point['fitting']] = points[point['fitting']] + 1

            for point in points:
                assert points[point] > 0, f"Количество точек с fitting={point} должно быть > 0, найдено {points[point]}"


@allure.feature('Partner API')
@allure.story('#27 Проверка paymentType - фильтрация на отделении нет приема ДС')
@allure.title('#27 Проверка paymentType - фильтрация на отделении нет приема ДС')
def test_set_shop_settings27(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-27-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 27 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений cashPayment=false и acquiring=false'):
            for point in response_json['points']:
                assert point['cashPayment'] is False and point['acquiring'] is False, \
                    f"В ответе должны быть отделения с cashPayment=false и acquiring=false " + str(point)


@allure.feature('Partner API')
@allure.story('#28 Проверка paymentType - фильтрация есть оплата наличными paymentType = 2')
@allure.title('#28 Проверка paymentType - фильтрация есть оплата наличными paymentType = 2')
def test_set_shop_settings28(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-28-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 28 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений cashPayment=true'):
            for point in response_json['points']:
                assert point['cashPayment'] is True, \
                    f"В ответе должны быть отделения с cashPayment=true " + str(point)


@allure.feature('Partner API')
@allure.story('#29 Проверка paymentType - фильтрация есть оплата банковской картой paymentType=3')
@allure.title('#29 Проверка paymentType - фильтрация есть оплата банковской картой paymentType=3')
def test_set_shop_settings29(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-29-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 29 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений acquiring=true'):
            for point in response_json['points']:
                assert point['acquiring'] is True, \
                    f"В ответе должны быть отделения с acquiring=true " + str(point)


@allure.feature('Partner API')
@allure.story('#30 Проверка paymentType - фильтрация на отделении нет приема ДС или оплата наличными или оплата банковской картой')
@allure.title('#30 Проверка paymentType - фильтрация на отделении нет приема ДС или оплата наличными или оплата банковской картой')
def test_set_shop_settings30(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-30-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 30 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":True}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        found = {0:0, 1:0, 2:0}

        with allure.step('Проверка наличия отделений'):
            for point in response_json['points']:
                assert (point['cashPayment'] is False and point['acquiring'] is False) or \
                       (point['cashPayment'] is True) or \
                       (point['acquiring'] is True), \
                    f"Не подходит по условиям поиска " + str(point)

                if point['cashPayment'] is False and point['acquiring'] is False:
                    found[0] = found[0] + 1
                if point['cashPayment'] is True:
                    found[1] = found[1] + 1
                if point['acquiring'] is True:
                    found[2] = found[2] + 1

        assert found[0] > 0, f"Ничего не было найдено по условию cashPayment=false и acquiring=false"
        assert found[1] > 0, f"Ничего не было найдено по условию cashPayment = true"
        assert found[2] > 0, f"Ничего не было найдено по условию acquiring = true"


@allure.feature('Partner API')
@allure.story('#31 Проверка typeServices - фильтрация прием заказов ИМ')
@allure.title('#31 Проверка typeServices - фильтрация прием заказов ИМ')
def test_set_shop_settings31(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-31-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 31 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений reception=true'):
            for point in response_json['points']:
                assert point['reception'] is True, \
                    f"В ответе должны быть отделения с reception=true " + str(point)


@allure.feature('Partner API')
@allure.story('#32 Проверка typeServices - фильтрация выдача заказов ИМ')
@allure.title('#32 Проверка typeServices - фильтрация выдача заказов ИМ')
def test_set_shop_settings32(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-32-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 32 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений issuanceBoxberry=true'):
            for point in response_json['points']:
                assert point['issuanceBoxberry'] is True, \
                    f"В ответе должны быть отделения с issuanceBoxberry=true " + str(point)


@allure.feature('Partner API')
@allure.story('#33 Проверка typeServices - фильтрация прием ПИП')
@allure.title('#33 Проверка typeServices - фильтрация прием ПИП')
def test_set_shop_settings33(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-33-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 33 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений receptionLap=true'):
            for point in response_json['points']:
                assert point['receptionLap'] is True, \
                    f"В ответе должны быть отделения с receptionLap=true " + str(point)


@allure.feature('Partner API')
@allure.story('#34 Проверка typeServices - фильтрация выдача ПИП')
@allure.title('#34 Проверка typeServices - фильтрация выдача ПИП')
def test_set_shop_settings34(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-34-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 34 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":True},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений deliveryLap=true'):
            for point in response_json['points']:
                assert point['deliveryLap'] is True, \
                    f"В ответе должны быть отделения с deliveryLap=true " + str(point)


@allure.feature('Partner API')
@allure.story('#35 Проверка typeServices - фильтрация возврат в ИМ')
@allure.title('#35 Проверка typeServices - фильтрация возврат в ИМ')
def test_set_shop_settings35(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-35-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 35 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":True},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений returnPackage=true'):
            for point in response_json['points']:
                assert point['returnPackage'] is True, \
                    f"В ответе должны быть отделения с returnPackage=true " + str(point)


@allure.feature('Partner API')
@allure.story('#36 Проверка typeServices - фильтрация МВ')
@allure.title('#36 Проверка typeServices - фильтрация МВ')
def test_set_shop_settings36(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-36-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 36 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":True},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений interRefunds=true'):
            for point in response_json['points']:
                assert point['interRefunds'] is True, \
                    f"В ответе должны быть отделения с interRefunds=true " + str(point)


@allure.feature('Partner API')
@allure.story('#37 Проверка typeServices - фильтрация выдача посылок международных ИМ')
@allure.title('#37 Проверка typeServices - фильтрация выдача посылок международных ИМ')
def test_set_shop_settings37(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-37-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 37 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":True}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений foreignOnlineStoresOnly=true'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['foreignOnlineStoresOnly'] is True, \
                    f"В ответе должны быть отделения с foreignOnlineStoresOnly=true " + str(point)


@allure.feature('Partner API')
@allure.story('#38 Проверка typeServices - фильтрация прием ПИП И выдача ПИП')
@allure.title('#38 Проверка typeServices - фильтрация прием ПИП И выдача ПИП')
def test_set_shop_settings38(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-38-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 38 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True},{"value":4,"enabled":True},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":True},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['receptionLap'] is True and point['deliveryLap'] is True, \
                    f"В ответе должны быть отделения receptionLap = true и deliveryLap = true " + str(point)


@allure.feature('Partner API')
@allure.story('#39 Проверка typeServices - фильтрация прием заказов ИМ или выдача заказов ИМ или МВ')
@allure.title('#39 Проверка typeServices - фильтрация прием заказов ИМ или выдача заказов ИМ или МВ')
def test_set_shop_settings39(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-39-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 39 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":True},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        found = {0: 0, 1: 0, 2: 0}

        with allure.step('Проверка наличия отделений'):
            for point in response_json['points']:
                assert point['reception'] is True or \
                       point['issuanceBoxberry'] is True or \
                       point['interRefunds'] is True, \
                    f"Не подходит по условиям поиска " + str(point)

                if point['reception'] is True:
                    found[0] = found[0] + 1
                if point['issuanceBoxberry'] is True:
                    found[1] = found[1] + 1
                if point['interRefunds'] is True:
                    found[2] = found[2] + 1

        assert found[0] > 0, f"Ничего не было найдено по условию reception = true"
        assert found[1] > 0, f"Ничего не было найдено по условию issuanceBoxberry = true"
        assert found[2] > 0, f"Ничего не было найдено по условию interRefunds = true"


@allure.feature('Partner API')
@allure.story('#40 Проверка country - фильтрация Россия')
@allure.title('#40 Проверка country - фильтрация Россия')
def test_set_shop_settings40(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-40-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 40 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":True},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['countryCode'] == 643, \
                    f"В ответе должны быть отделения countryCode = 643 (Россия) " + str(point)


@allure.feature('Partner API')
@allure.story('#41 Проверка country - фильтрация Россия или Казахстан')
@allure.title('#41 Проверка country - фильтрация Россия или Казахстан')
def test_set_shop_settings41(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-41-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 41 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":True},{"value":804,"enabled":False},{"value":398,"enabled":True}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        found = {0: 0, 1: 0}

        with allure.step('Проверка наличия отделений'):
            for point in response_json['points']:
                assert point['countryCode'] == 643 or \
                       point['countryCode'] == 398, \
                    f"Не подходит countryCode " + str(point)

                if point['countryCode'] == 643:
                    found[0] = found[0] + 1
                if point['countryCode'] == 398:
                    found[1] = found[1] + 1

        assert found[0] > 0, f"Ничего не было найдено по условию countryCode = 643 (Россия)"
        assert found[1] > 0, f"Ничего не было найдено по условию countryCode = 398 (Казахстан)"


@allure.feature('Partner API')
@allure.story('#42 Проверка status - фильтрация по статусу Создано')
@allure.title('#42 Проверка status - фильтрация по статусу Создано')
def test_set_shop_settings42(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-42-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 42 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":True},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['status'] == 1, \
                    f"В ответе должны быть отделения с status=1 " + str(point)


@allure.feature('Partner API')
@allure.story('#43 Проверка status - фильтрация по статусу Открыто')
@allure.title('#43 Проверка status - фильтрация по статусу Открыто')
def test_set_shop_settings43(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-43-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 43 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":True},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['status'] == 2, \
                    f"В ответе должны быть отделения с status=2 " + str(point)


@allure.feature('Partner API')
@allure.story('#44 Проверка status - фильтрация по статусу Временно закрыто')
@allure.title('#44 Проверка status - фильтрация по статусу Временно закрыто')
def test_set_shop_settings44(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-44-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 44 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":True},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['status'] == 3, \
                    f"В ответе должны быть отделения с status=3 " + str(point)


@allure.feature('Partner API')
@allure.story('#45 Проверка status - фильтрация по статусу Временно закрыто')
@allure.title('#45 Проверка status - фильтрация по статусу Временно закрыто')
def test_set_shop_settings45(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-45-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 45 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":True}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['status'] == 4, \
                    f"В ответе должны быть отделения с status=4 " + str(point)


@allure.feature('Partner API')
@allure.story('#46 Проверка status - фильтрация по статусу Создано или Открыто или Закрыто')
@allure.title('#46 Проверка status - фильтрация по статусу Создано или Открыто или Закрыто')
def test_set_shop_settings46(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-46-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 46 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":True},{"value":2,"enabled":True},{"value":3,"enabled":False},{"value":4,"enabled":True}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        found = {1: 0, 2: 0, 4: 0}

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['status'] == 1 or \
                       point['status'] == 2 or \
                       point['status'] == 4,\
                    f"status должен быть 1,2 или 4 " + str(point)

                found[point['status']] = found[point['status']] + 1

        assert found[1] > 0, f"Ничего не было найдено по условию status = 1"
        assert found[2] > 0, f"Ничего не было найдено по условию status = 2"
        assert found[4] > 0, f"Ничего не было найдено по условию status = 4"


@allure.feature('Partner API')
@allure.story('#47 Проверка identification - фильтрация паспорт')
@allure.title('#47 Проверка identification - фильтрация паспорт')
def test_set_shop_settings47(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-47-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 47 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":True},{"value":2,"enabled":False}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['identificationByPassport'] is True, \
                    f"В ответе должны быть отделения с identificationByPassport = True " + str(point)


@allure.feature('Partner API')
@allure.story('#48 Проверка identification - фильтрация СМС код')
@allure.title('#48 Проверка identification - фильтрация СМС код')
def test_set_shop_settings48(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-48-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 48 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":False},{"value":2,"enabled":True}],"isAnd":True}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['identificationBySMSCode'] is True, \
                    f"В ответе должны быть отделения с identificationBySMSCode = True " + str(point)


@allure.feature('Partner API')
@allure.story('#49 Проверка identification - фильтрация паспорт и СМС код')
@allure.title('#49 Проверка identification - фильтрация паспорт и СМС код')
def test_set_shop_settings49(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-49-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 49 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True}],"isAnd":True}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['identificationBySMSCode'] is True and point['identificationByPassport'] is True, \
                    f"В ответе должны быть отделения с identificationBySMSCode = True и identificationByPassport = True " + str(point)


@allure.feature('Partner API')
@allure.story('#50 Проверка identification - фильтрация паспорт или СМС код')
@allure.title('#50 Проверка identification - фильтрация паспорт или СМС код')
def test_set_shop_settings50(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, string_helper):
    shop_postfix = string_helper.get_random_string(10)
    imCode = f"at-test-set-shop-settings-50-{shop_postfix}"

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test Set Shop Settings 50 ({shop_postfix})",
            'clientType': 'LK_IM'
        }
    )

    filter = [{"id":"1","values":[{"value":0,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":1,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":2,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":3,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]},{"value":4,"enabled":False,"values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}]}],"isAnd":False},{"id":"2","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":True},{"id":"3","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"4","values":[{"value":0,"enabled":False},{"value":1,"enabled":False},{"value":2,"enabled":False}],"isAnd":False},{"id":"5","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False}],"isAnd":False},{"id":"6","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False},{"value":5,"enabled":False},{"value":6,"enabled":False},{"value":7,"enabled":False}],"isAnd":False},{"id":7,"values":[{"value":643,"enabled":False},{"value":804,"enabled":False},{"value":398,"enabled":False}],"isAnd":False},{"id":"8","values":[{"value":1,"enabled":False},{"value":2,"enabled":False},{"value":3,"enabled":False},{"value":4,"enabled":False}],"isAnd":False},{"id":9,"values":[{"value":1,"enabled":True},{"value":2,"enabled":True}],"isAnd":False}]
    PartnerServiceAPI.SetShopSettings(imCode, filter)
    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint({'imCode': imCode})
        response_json = response.json()
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        found = {0: 0, 1: 0}

        with allure.step('Проверка наличия отделений'):
            assert 'points' in response_json, f"Не найден массив points в ответе"
            for point in response_json['points']:
                assert point['identificationBySMSCode'] is True or point['identificationByPassport'] is True, \
                    f"В ответе должны быть отделения с identificationBySMSCode = True или identificationByPassport = True " + str(point)

                if point['identificationBySMSCode'] is True:
                    found[0] = found[0] + 1
                if point['identificationByPassport'] is True:
                    found[1] = found[1] + 1

        assert found[0] > 0, f"Ничего не было найдено по условию identificationBySMSCode = true"
        assert found[1] > 0, f"Ничего не было найдено по условию identificationByPassport = true"
