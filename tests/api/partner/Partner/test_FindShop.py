import allure
import pytest
from library.common.db import *


@allure.feature('Partner API')
@allure.story('Проверка наличия хотя бы одного параметра в searchParameter')
@allure.title('Проверка наличия хотя бы одного параметра в searchParameter')
def test_check_at_least_one_param(PartnerServiceAPI, PartnerAPIHelper):
    response = PartnerServiceAPI.FindShop({})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 500, {
        "error": "invalid field SearchParameter: message must exist",
        "code": 2,
        "message": "invalid field SearchParameter: message must exist"
    })


@allure.feature('Partner API')
@allure.story('Проверка наличия limit, offset')
@allure.title('Проверка наличия limit, offset')
@pytest.mark.parametrize("param,absence", [('limit', 'Offset'), ('offset', 'Limit')])
def test_check_limit_and_offset_required(PartnerServiceAPI, PartnerAPIHelper, param, absence):
    response = PartnerServiceAPI.FindShop({'searchParameter.shopName': 'aaa', param: 10})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 500, {
        "error": f"invalid field {absence}: message must exist",
        "code": 2,
        "message": f"invalid field {absence}: message must exist"
    })


@allure.feature('Partner API')
@allure.story('Проверка длины строк shopName и legalEntityName < 3')
@allure.title('Проверка длины строк shopName и legalEntityName < 3')
@pytest.mark.parametrize("param", ['searchParameter.shopName', 'searchParameter.legalEntityName'])
def test_check_short_params(PartnerServiceAPI, PartnerAPIHelper, param):
    response = PartnerServiceAPI.FindShop({param: 'aa', 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 500, {
        "error": f"поле {param} должно быть размером не менее 3 символов",
        "code": 2,
        "message": f"поле {param} должно быть размером не менее 3 символов"
    })


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "code"')
@allure.title('Передан параметр поиска "code"')
def test_find_shop_by_code(PartnerServiceAPI, PartnerAPIHelper):
    response = PartnerServiceAPI.FindShop({'searchParameter.code': f"BBR5001670", 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 200, {
        "shops":
            [{"code": "BBR5001670", "name": "ИП Кузнецова Елена Николаевна"}]
    })


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "nameShop"')
@allure.title('Передан параметр поиска "nameShop"')
@pytest.mark.parametrize("name", ['Лагашкина Каринэ', 'ИП Лагашкина Каринэ'])
def test_find_shop_by_name_shop(PartnerServiceAPI, PartnerAPIHelper, name):
    response = PartnerServiceAPI.FindShop({'searchParameter.shopName': name, 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 200, {
        "shops":
            [{"code": "BBR5001667", "name": "ИП Лагашкина Каринэ Левоновна"}]
    })


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "foreign"')
@allure.title('Передан параметр поиска "foreign"')
@pytest.mark.parametrize("value", ['true', 'false'])
def test_find_shop_by_foreign(PartnerServiceAPI, PartnerAPIHelper, value):
    response = PartnerServiceAPI.FindShop({'searchParameter.foreign': value, 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponse200NotEmptyJson(response)
    json = response.json()
    with allure.step('Проверка данных в ответе'):
        assert 'shops' in json, f"shops не присутствует в ответе"
        db = DBClient('partner')
        for shop in response.json()['shops']:
            with allure.step(f"Проверка магазина {shop['name']}"):
                shopDB = db.fetchOne(f"SELECT * FROM shops WHERE code='{shop['code']}' LIMIT 1")
                if value == 'true':
                    assert shopDB['is_foreign'] is True, f"Магазин не помечен как зарубежный в БД"
                else:
                    assert shopDB['is_foreign'] is False, f"Магазин помечен как зарубежный в БД"


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "partnerType"')
@allure.title('Передан параметр поиска "partnerType"')
def test_find_shop_by_partner_type(PartnerServiceAPI, PartnerAPIHelper):
    response = PartnerServiceAPI.FindShop({'searchParameter.partnerType': 2, 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 200, {
        "shops":
            [{"code": "BBR4766294", "name": "ООО Раннее развитие"}]
    })


@allure.feature('Partner API')
@allure.story('Переданы несколько параметров поиска в объекте searchParameter')
@allure.title('Переданы несколько параметров поиска в объекте searchParameter')
def test_find_shop_by_several_exists(PartnerServiceAPI, PartnerAPIHelper):
    response = PartnerServiceAPI.FindShop({
        'searchParameter.partnerType': 1,
        'searchParameter.foreign': 'false',
        'searchParameter.shopName': 'ООО',
        'limit': 10,
        'offset': 0
    })
    PartnerAPIHelper.CheckResponse200NotEmptyJson(response)
    json = response.json()
    with allure.step('Проверка данных в ответе'):
        assert 'shops' in json, f"shops не присутствует в ответе"
        db = DBClient('partner')
        for shop in response.json()['shops']:
            with allure.step(f"Проверка магазина {shop['name']}"):
                shopDB = db.fetchOne(f"SELECT * FROM shops WHERE code='{shop['code']}' LIMIT 1")
                partnerDB = db.fetchOne(f"SELECT * FROM partners WHERE code='{shop['code']}' LIMIT 1")
                assert shopDB['is_foreign'] is False, f"Магазин помечен как зарубежный в БД"
                assert partnerDB['type'] == 1, f"Тип партнера должен быть равен 1"
                assert 'ООО' in shop['name'], f"В названии нет ООО"


@allure.feature('Partner API')
@allure.story('Передан параметр поиска "nameLegalEntity"')
@allure.title('Передан параметр поиска "nameLegalEntity"')
@pytest.mark.parametrize("query", ['Тестовая', 'Легал'])
def test_find_shop_by_name_name_legal_entity(PartnerServiceAPI, PartnerAPIHelper, query):
    with allure.step('Подготовка тестовых данных'):
        db = DBClient('partner')
        row = db.fetchOne("SELECT count(*) as cnt FROM legal_entities WHERE code = 'BBR4238648'")
        if row['cnt'] == 0:
            db.insert("INSERT INTO legal_entities (code, name, inn, kpp, bik, bank_name, account_number, account_corr, address)"
                      "VALUES ('BBR4238648', 'Тестовая Легал Запись', '888', '888', '888', 'АльфаБанк', '40702810400430121198-888', '40702810400430121198-888', 'Кипр')")
        else:
            db.update("UPDATE legal_entities SET name = 'Тестовая Легал Запись' WHERE code = 'BBR4238648'")

    response = PartnerServiceAPI.FindShop({'searchParameter.legalEntityName': query, 'limit': 10, 'offset': 0})
    PartnerAPIHelper.CheckResponseCodeAndJson(response, 200, {
        "shops":
            [{"code": "BBR4238648", "name": '5LB'}]
    })
