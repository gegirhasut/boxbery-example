import allure
import pytest
from library.common.db import *


@allure.feature('Catalogs API')
@allure.story('Запрос списка городов')
@allure.title('Запрос списка городов')
def test_full_list(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity()
    CatalogsAPIHelper.CheckResponse200HasCities(response)

    with allure.step(f"Проверка что функция не вернула не активный город"):
        with allure.step(f"Поиск не активного города"):
            db = DBClient('catalog')
            notActiveCity = db.fetchOne("SELECT * FROM cities WHERE active='f' LIMIT 1")
            cities = CatalogsAPIHelper.Cities(response)
            item = next((item for item in cities if item["cityCode"] == notActiveCity["city_code"]), None)
        with allure.step(f"Проверка что города не было в списке"):
            assert item is None, f"Город найден {notActiveCity['name']}, но не должен"


@allure.feature('Catalogs API')
@allure.story('Запрос списка городов с проверкой всех параметров')
@allure.title('Запрос списка городов с проверкой всех параметров')
def test_full_list_with_fields(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity()
    CatalogsAPIHelper.CheckResponse200HasCities(response, True)


@allure.feature('Catalogs API')
@allure.story('Проверка фильтра')
@allure.title('Проверка фильтра')
@pytest.mark.parametrize("filters", [
    ({'countryCode': 804, 'pickupPoint': 'true'}),
    ({'countryCode': 804, 'pickupPoint': 'false'}),
    ({'countryCode': 643, 'reception': 'true'}),
    ({'countryCode': 643, 'reception': 'false'}),
    ({'countryCode': 804, 'deliveryLap': 'true'}),
    ({'countryCode': 804, 'deliveryLap': 'false'}),
    ({'countryCode': 643, 'receptionLap': 'true'}),
    ({'countryCode': 804, 'receptionLap': 'false'}),
    ({'countryCode': 643, 'courierDelivery': 'true'}),
    ({'countryCode': 804, 'courierDelivery': 'false'}),
    ({'countryCode': 804, 'courierReception': 'true'}),
    ({'countryCode': 804, 'courierReception': 'false'}),
    ({'countryCode': 643, 'fiasID': '862622ef-3340-4137-bbcb-7cbf4fe8cc94'}),
    ({'cityCodes': 'Н00168975'}),
    ({'cityCodes': 'Н00168975', 'pickupPoint': 'false', 'reception': 'false', 'deliveryLap': 'false',
      'receptionLap': 'false', 'courierDelivery': 'false', 'courierReception': 'false'}),
])
def test_set(CatalogsServiceAPI, CatalogsAPIHelper, filters):
    response = CatalogsServiceAPI.ListCity(filters)
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка что все найденные города с верными свойствами"):
        for city in cities:
            with allure.step(f"Проверка {city['name']}"):
                #allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                for param in filters:
                    paramName = param
                    if param == 'fiasID':
                        paramName = 'fiasId'
                    if param == 'cityCodes':
                        paramName = 'cityCode'
                    value = filters[param]
                    if value == 'true':
                        value = True
                    if value == 'false':
                        value = False
                    with allure.step(f"Проверка наличия {paramName}"):
                        assert paramName in city
                    with allure.step(f"Проверка значения {paramName}"):
                        assert city[paramName] == value


@allure.feature('Catalogs API')
@allure.story('Проверка isBoxberry фильтра true')
@allure.title('Проверка isBoxberry фильтра true')
def test_isboxberry_filter(CatalogsServiceAPI, CatalogsAPIHelper):
    isBoxberry = 'true' # 'false' не используется
    with allure.step(f"Запрос списка городов с фильтром isBoxberry = {isBoxberry}"):
        response = CatalogsServiceAPI.ListCity(({'countryCode': 643, 'isBoxberry': isBoxberry}))

    with allure.step(f"Проверка результатов на isBoxberry = {isBoxberry}"):
        CatalogsAPIHelper.CheckResponse200HasCities(response)
        cities = CatalogsAPIHelper.Cities(response)
        for city in cities:
            with allure.step(f"Проверка города {city['name']}"):
                # allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                with allure.step(f"Проверка условия (pickup_point = true OR courier_delivery = true OR terminal = true OR postamat = true OR russian_post_delivery = true)"):
                    assert city['pickupPoint'] == True or city['courierDelivery'] == True or city['terminal'] == True or city['postamat'] == True or city['russianPostDelivery'] == True


@allure.feature('Catalogs API')
@allure.story('Запрос города (Н00165202, Авангард) и проверка всех его свойств')
@allure.title('Запрос города (Н00165202, Авангард) и проверка всех его свойств')
def test_city_all_fields(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity({'cityCodes': 'Н00165202'})

    CatalogsAPIHelper.CheckResponse200WithCity(response, {
         "cityCode": "Н00165202",
         "name": "Авангард",
         "receptionLap": False,
         "deliveryLap": False,
         "reception": False,
         "pickupPoint": False,
         "courierDelivery": False,
         "foreignReceptionReturns": False,
         "terminal": False,
         "countryCode": 804,
         "region": "Одесская",
         "postamat": False,
         "courierReception": False,
         "russianPostDelivery": False
      })


@allure.feature('Catalogs API')
@allure.story('Фильтр "Выдача заказов  российских ИМ" - filter.pickupPointRF')
@allure.title('Фильтр "Выдача заказов  российских ИМ" - filter.pickupPointRF')
@pytest.mark.parametrize("filters", [
    ({'pickupPointRF': 'true'}),
    ({'pickupPointRF': 'false'}),
])
def test_pickupPointRF(CatalogsServiceAPI, CatalogsAPIHelper, filters):
    response = CatalogsServiceAPI.ListCity(filters)
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка что все найденные города с pickupPointRF"):
        for city in cities:
            assert 'pickupPointRF' in city, f"pickupPointRF не присутствует: " + str(city)
            if filters['pickupPointRF'] == 'true':
                assert city['pickupPointRF'] is True, f"pickupPointRF не True: " + str(city)
            else:
                assert city['pickupPointRF'] is False, f"pickupPointRF не False: " + str(city)


@allure.feature('Catalogs API')
@allure.story('#3 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#3 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_3(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity({'cityCodes': 'Н00168975'})
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие города Н00168975"):
        for city in cities:
            assert city['cityCode'] == 'Н00168975', f"cityCode не равен Н00168975: " + str(city)


@allure.feature('Catalogs API')
@allure.story('#4 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#4 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_4(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&filter.cityCodes=Н00181893')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    cities_found = {'Н00168975' : 0, 'Н00181893': 0}

    for city in cities:
        assert city['cityCode'] in cities_found, f"{city['cityCode']} не ожидалось в результирующем наборе"
        cities_found[city['cityCode']] = cities_found[city['cityCode']] + 1

    with allure.step(f"Проверка на наличие городов Н00168975, Н00181893"):
        assert cities_found['Н00168975'] == 1, f"cityCode не равен Н00168975: " + str(cities[0])
        assert cities_found['Н00181893'] == 1, f"cityCode не равен Н00181893: " + str(cities[1])


@allure.feature('Catalogs API')
@allure.story('#5 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#5 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_5(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=AAAAA')
    CatalogsAPIHelper.CheckResponse200EmptyJson(response)


@allure.feature('Catalogs API')
@allure.story('#6 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#6 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_6(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&filter.cityCodes=ABCDEFG')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие города Н00168975"):
        for city in cities:
            assert city['cityCode'] == 'Н00168975', f"cityCode не равен Н00168975: " + str(city)
        assert len(cities) == 1, f"В ответе должен быть только 1 город: " + str(cities)


@allure.feature('Catalogs API')
@allure.story('#7 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#7 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_7(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&filter.cityCodes=Н00168975')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие городов Н00168975, Н00181893"):
        assert cities[0]['cityCode'] == 'Н00168975', f"cityCode не равен Н00168975: " + str(cities[0])
        assert len(cities) == 1, f"В ответет только 1 город" + str(cities)


@allure.feature('Catalogs API')
@allure.story('#8 Фильтр список кодов города Boxberry - filter.cityCodes')
@allure.title('#8 Фильтр список кодов города Boxberry - filter.cityCodes')
def test_cityCode_cityCodes_8(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&filter.cityCodes=Н00181893')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    cities_found = {'Н00168975': 0, 'Н00181893': 0}

    for city in cities:
        assert city['cityCode'] in cities_found, f"{city['cityCode']} не ожидалось в результирующем наборе"
        cities_found[city['cityCode']] = cities_found[city['cityCode']] + 1

    with allure.step(f"Проверка на наличие городов Н00168975, Н00181893"):
        assert cities_found['Н00168975'] == 1, f"cityCode не равен Н00168975: " + str(cities[0])
        assert cities_found['Н00181893'] == 1, f"cityCode не равен Н00181893: " + str(cities[1])


@allure.feature('Catalogs API')
@allure.story('#9 параметры в returnedField и данных ответа')
@allure.title('#9 параметры в returnedField и данных ответа')
def test_cityCode_cityCodes_9(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&returnedFields=C_PICKUP_POINT_RF')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие pickupPointRF в ответе и только"):
        assert len(cities) == 1, f"В ответе только 1 город: " + str(cities)
        assert len(cities[0]) == 1, f"В городе только одно свойство: " + str(cities[0])
        assert 'pickupPointRF' in cities[0], f"pickupPointRF присутствует в ответе: " + str(cities[0])


@allure.feature('Catalogs API')
@allure.story('#10 параметры в returnedField и данных ответа')
@allure.title('#10 параметры в returnedField и данных ответа')
def test_cityCode_cityCodes_10(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00168975&returnedFields=C_NAME_ENG')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие pickupPointRF в ответе и только"):
        assert len(cities) == 1, f"В ответе только 1 город: " + str(cities)
        assert len(cities[0]) == 0, f"В городе нет свойств: " + str(cities[0])
        assert 'nameEng' not in cities[0], f"nameEng отсутствует в ответе: " + str(cities[0])

    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00194074&returnedFields=C_NAME_ENG')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие pickupPointRF в ответе и только"):
        assert len(cities) == 1, f"В ответе только 1 город: " + str(cities)
        assert len(cities[0]) == 1, f"В городе есть 1 свойство: " + str(cities[0])
        assert 'nameEng' in cities[0], f"nameEng присутствует в ответе: " + str(cities[0])

    response = CatalogsServiceAPI.ListCity('filter.cityCodes=Н00194125&returnedFields=C_NAME_ENG')
    CatalogsAPIHelper.CheckResponse200HasCities(response)
    cities = CatalogsAPIHelper.Cities(response)
    with allure.step(f"Проверка на наличие pickupPointRF в ответе и только"):
        assert len(cities) == 1, f"В ответе только 1 город: " + str(cities)
        assert len(cities[0]) == 1, f"В городе есть 1 свойство: " + str(cities[0])
        assert 'nameEng' in cities[0], f"nameEng присутствует в ответе: " + str(cities[0])
        assert cities[0]['nameEng'] == 'Badulla', "Отстутствует верное значение name_eng = Badulla: " + str(cities[0])
