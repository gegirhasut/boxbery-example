import allure


@allure.feature('Catalogs API')
@allure.story('Список фильтров для ПВЗ')
@allure.title('Список фильтров для ПВЗ')
def test_list_filter(CatalogsServiceAPI, CatalogsAPIHelper):
    response = CatalogsServiceAPI.ListFilter()
    CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

    response_body = response.json()
    filters = {"filters": [
        {"filterId":1,"filterName":"Тип партнера","values":[{"name":"LK Boxberry","groupType":1,"groupData":[{"name":"Терминал","value":3},{"name":"Постомат","value":2},{"name":"Отделение","value":1}]},{"name":"Beeline","value":1,"groupType":1,"groupData":[{"name":"Терминал","value":3},{"name":"Постомат","value":2},{"name":"Отделение","value":1}]},{"name":"PickPoint","value":2,"groupType":1,"groupData":[{"name":"Терминал","value":3},{"name":"Постомат","value":2},{"name":"Отделение","value":1}]},{"name":"Qiwi","value":3,"groupType":1,"groupData":[{"name":"Терминал","value":3},{"name":"Постомат","value":2},{"name":"Отделение","value":1}]},{"name":"Lamoda","value":4,"groupType":1,"groupData":[{"name":"Терминал","value":3},{"name":"Постомат","value":2},{"name":"Отделение","value":1}]}]},
        {"filterId":2,"filterName":"Вид выдачи","withAnd":True,"values":[{"name":"без вскрытия","value":1},{"name":"со вскрытием","value":2},{"name":"частичная выдача","value":3}]},
        {"filterId":4,"filterName":"Примерка","values":[{"name":"нет примерки"},{"name":"Частичная примерка","value":1},{"name":"Полная примерка","value":2}]},
        {"filterId":5,"filterName":"Оплата на отделении","values":[{"name":"нет приема ДС","value":1},{"name":"оплата наличными","value":2},{"name":"оплата банковской картой","value":3}]},
        {"filterId":6,"filterName":"Услуги отделения","withAnd":True,"values":[{"name":"Прием заказов от ИМ","value":1},{"name":"Выдача заказов ИМ","value":2},{"name":"Прием ПИП","value":3},{"name":"Выдача ПИП","value":4},{"name":"Возврат в интернет-магазин","value":5},{"name":"Международный возврат","value":6},{"name":"Выдача посылок международных ИМ","value":7}]},
        {"filterId":9,"filterName":"Подтверждение личности","withAnd":True,"values":[{"name":"Идентификация по паспорту","value":1},{"name":"Идентификация по смс-коду","value":2}]},
        {"filterId":3,"filterName":"Тип транспортировки","withAnd":True,"values":[{"name":"автотранспорт","value":1},{"name":"авиадоставка","value":2},{"name":"ж/д","value":3}]},
        {"filterId":7,"filterName":"Страна","values":[{"name":"Казахстан","value":398},{"name":"Россия","value":643},{"name":"Украина","value":804}]},
        {"filterId":8,"filterName":"Статус","values":[{"name":"Открыто","value":2},{"name":"Временно закрыто","value":3},{"name":"Закрыто","value":4},{"name":"Создано","value":1}]}
    ]}
    with allure.step('Проверка фильтров'):
        with allure.step(f"Проверка количества фильтров = {str(len(filters['filters']))}"):
            assert len(response_body['filters']) == len(filters['filters'])
        for the_filter in filters['filters']:
            with allure.step(f"Проверка фильтра {the_filter['filterName']}"):
                for i in range(len(response_body['filters'])):
                    if response_body['filters'][i]['filterId'] == the_filter['filterId']:
                        for the_property in the_filter:
                            with allure.step(f"Проверка {the_property}"):
                                if not isinstance(the_filter[the_property], list):
                                    assert the_filter[the_property] == response_body['filters'][i][the_property],\
                                        f"Свойство: {the_property} не совпадает. Фильтр: " + str(the_filter) + ". В ответе: " + response_body['filters'][i]
                                else:
                                    assert len(the_filter[the_property]) == len(response_body['filters'][i][the_property]),\
                                        f"Количество не совпадает. Свойство {the_property}. Фильтр: " + str(the_filter) +\
                                        ". В ответе: " + str(response_body['filters'][i])


