from library.common.db import *
import allure
from datetime import date
import datetime
import pytest


@allure.feature('Catalogs API')
@allure.story('Поиск отделений')
@allure.title('Поиск отделений')
@pytest.mark.parametrize("clientType,filters", [
    ('LK_IM', {'imCode': "at-lppf", 'filter.pointCodes': '82621'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.cityCode': '41'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.acceptPayments': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.acceptPayments': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.reception': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.reception': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.issuanceBoxberry': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.issuanceBoxberry': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.receptionLap': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.receptionLap': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.deliveryLap': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.deliveryLap': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.enableFitting': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.enableFitting': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.foreignOnlineStoresOnly': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.foreignOnlineStoresOnly': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.acquiring': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.acquiring': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.interRefunds': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.interRefunds': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.expressReception': 'true'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.expressReception': 'false'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.search': 'Симферополь Гайдара_8262_С'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.search': '295026, Симферополь г, Гайдара ул, д.12'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.search': 'Проспект Просвещения'}),
    ('LK_IM', {'imCode': "at-lppf", 'filter.search': 'Остановка: Проспект просвещения.\nПримерное расстояние от остановки до Отделения  -  300 метров.\nЖилой 12-ти этажный дом с административными помещениями.\nТурфирма \"Пилигрим\".\n1 этаж.'}),
])
def test_list_pick_point_filter(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI, clientType, filters):
    PartnerServiceAPI.CreateShop(
        {
            'clientCode': filters['imCode'],
            'name': f"AT: Test List Pick Point (filters)",
            'clientType': clientType
        }
    )

    filters['page.limit'] = 20
    filters['page.offset'] = 0

    with allure.step('Поиск и проверка результатов'):
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

        with allure.step('Проверка points в ответе'):
            response_json = response.json()
            with allure.step('Наличие points в ответе'):
                assert 'points' in response_json, f"Нет points в ответе"
            with allure.step('Количество points > 0'):
                assert len(response_json['points']) > 0, f"Пустой points в ответе"

        with allure.step('Проверка соответствия фильтрам'):
            for point in response_json['points']:
                with allure.step(f"Проверка {point['name']}"):
                    # allure.attach(str(point), 'POINT DATA', allure.attachment_type.JSON)
                    for theFilter in filters:
                        if theFilter == 'imCode' or theFilter == 'page.limit' or theFilter == 'page.offset':
                            continue

                        theFilterName = theFilter.replace('filter.', '')
                        theFilterValue = filters[theFilter]
                        if theFilterValue == 'true':
                            theFilterValue = True
                        if theFilterValue == 'false':
                            theFilterValue = False

                        with allure.step(f"Проверка {theFilterName}"):
                            if theFilterName == 'acceptPayments':
                                assert 'cashPayment' in point, f"Не найдены данные cashPayment"
                                assert 'acquiring' in point, f"Не найдены данные acquiring"
                                if filters[theFilter] == 'true':
                                    assert point['cashPayment'] is True or point['acquiring'] is True, \
                                        f"cashPayment или acquiring должно быть true"
                            elif theFilterName == 'enableFitting':
                                assert 'fitting' in point, f"Не найдены данные fitting"
                                if filters[theFilter] == 'true':
                                    assert point['fitting'] == 1 or point['fitting'] == 2, \
                                        f"fitting должно быть 1 или 2"
                                if filters[theFilter] == 'false':
                                    assert point['fitting'] == 0, \
                                        f"fitting должно быть равно 0"
                            elif theFilterName == 'search':
                                assert 'addressFull' in point, f"Не найдены данные addressFull"
                                assert 'name' in point, f"Не найдены данные name"
                                assert 'tripDescription' in point, f"Не найдены данные tripDescription"

                                assert point['name'].find(filters[theFilter]) >= 0 or \
                                       point['addressFull'].find(filters[theFilter]) >= 0 or \
                                       point['tripDescription'].find(filters[theFilter]) >= 0 or \
                                       ('metro' in point and point['metro'].find(filters[theFilter]) >= 0), \
                                       f"{filters[theFilter]} должно присутствовать в name || addressFull || tripDescription || metro"
                            elif theFilterName == 'pointCodes':
                                assert 'pointCode' in point, f"Не найдены данные pointCode"
                                assert point['pointCode'] == theFilterValue, \
                                    f"Значение pointCode не соответствует критерию поиска"
                            else:
                                assert theFilterName in point, f"Не найдены данные {theFilterName}"
                                assert point[theFilterName] == theFilterValue, \
                                    f"Значение {theFilterName} не соответствует критерию поиска"


@allure.feature('Catalogs API')
@allure.story('Поиск отделений открытых в выходные')
@allure.title('Поиск отделений открытых в выходные')
def test_list_pick_point_weekends(CatalogsServiceAPI, CatalogsAPIHelper, PartnerServiceAPI):
    imCode = 'at-lppf-weekends'
    pointCode = '17482'
    db = DBClient('catalog')
    filters = {'imCode': imCode, 'filter.pointCodes': pointCode, 'filter.isOpenOnWeekends': 'true'}

    PartnerServiceAPI.CreateShop(
        {
            'clientCode': imCode,
            'name': f"AT: Test List Pick Point (filters weekends)",
            'clientType': 'LK_IM'
        }
    )

    with allure.step(f"Проверить с пустым расписанием"):
        db.delete(f"DELETE FROM schedules WHERE point_code = '{pointCode}'")
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
        assert int(response.json()['total']) == 0, f"total должно быть равно 0"

    with allure.step(f"Проверить с субботой"):
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','2020-10-01',6,'11:00:00','21:00:00')")
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
        assert int(response.json()['total']) > 0, f"total должно быть больше 0"

    with allure.step(f"Проверить с субботой и воскресеньем"):
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','2020-10-01',7,'11:00:00','21:00:00')")
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
        assert int(response.json()['total']) > 0, f"total должно быть больше 0"

    with allure.step(f"Проверить с temporary = true"):
        db.update(f"UPDATE schedules SET temporary = 't' WHERE point_code = '{pointCode}'")
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
        assert int(response.json()['total']) == 0, f"total должно быть равно 0"

    with allure.step(f"Проверить с dateStart > текущей"):
        tomorrow = (date.today() + datetime.timedelta(days=1)).strftime("%Y-%m-%d")
        db.update(f"UPDATE schedules SET temporary = 'f', date_start = '{tomorrow}' WHERE point_code = '{pointCode}'")
        response = CatalogsServiceAPI.ListPickPoint(filters)
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
        assert int(response.json()['total']) == 0, f"total должно быть равно 0"


@allure.feature('Catalogs API')
@allure.story('Поиск отделений по GEO')
@allure.title('Поиск отделений по GEO')
def test_list_pick_point_geo(CatalogsServiceAPI, CatalogsAPIHelper):
    imCode = 'at-lppf-weekends'

    filters = {
        'imCode': imCode,
        'filter.geoArea.leftTopCoords.latitude': 55.179344177246094,
        'filter.geoArea.leftTopCoords.longitude': 60.33099365234375,
        'filter.geoArea.rightBottomCoords.latitude': 54.179344177246094,
        'filter.geoArea.rightBottomCoords.longitude': 61.33099365234375
    }

    response = CatalogsServiceAPI.ListPickPoint(filters)
    CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)
    response_json = response.json()

    with allure.step(f"Проверка latitude, longitude в заданных координатах"):
        for point in response_json['points']:
            with allure.step(f"Проверка {point['name']} ({point['latitude']}, {point['longitude']})"):
                assert point['latitude'] <= filters['filter.geoArea.leftTopCoords.latitude'],\
                    f"latitude за пределами leftTopCoords.latitude"
                assert point['longitude'] >= filters['filter.geoArea.leftTopCoords.longitude'], \
                    f"longitude за пределами leftTopCoords.longitude"

                assert point['latitude'] >= filters['filter.geoArea.rightBottomCoords.latitude'], \
                    f"latitude за пределами rightBottomCoords.latitude"
                assert point['longitude'] <= filters['filter.geoArea.rightBottomCoords.longitude'], \
                    f"longitude за пределами rightBottomCoords.longitude"


@allure.feature('Catalogs API')
@allure.story('Поиск отделений по GEO (большая область)')
@allure.title('Поиск отделений по GEO (большая область)')
def test_list_pick_point_geo_big(CatalogsServiceAPI):
    imCode = 'at-lppf-weekends'

    filters = {
        'imCode': imCode,
        'filter.geoArea.leftTopCoords.latitude': 76.179344177246094,
        'filter.geoArea.leftTopCoords.longitude': 40.33099365234375,
        'filter.geoArea.rightBottomCoords.latitude': 45.179344177246094,
        'filter.geoArea.rightBottomCoords.longitude': 81.33099365234375,
        'page.limit': 10,
        'page.offset': 0
    }

    response = CatalogsServiceAPI.ListPickPoint(filters)
    response_json = response.json()

    with allure.step(f"Проверка что 10 результатов и latitude, longitude в заданных координатах"):
        for point in response_json['points']:
            with allure.step(f"Проверка {point['name']} ({point['latitude']}, {point['longitude']})"):
                assert point['latitude'] <= filters['filter.geoArea.leftTopCoords.latitude'],\
                    f"latitude за пределами leftTopCoords.latitude"
                assert point['longitude'] >= filters['filter.geoArea.leftTopCoords.longitude'], \
                    f"longitude за пределами leftTopCoords.longitude"

                assert point['latitude'] >= filters['filter.geoArea.rightBottomCoords.latitude'], \
                    f"latitude за пределами rightBottomCoords.latitude"
                assert point['longitude'] <= filters['filter.geoArea.rightBottomCoords.longitude'], \
                    f"longitude за пределами rightBottomCoords.longitude"


@allure.feature('Catalogs API')
@allure.story('Поиск отделений по GEO + page, offset')
@allure.title('Поиск отделений по GEO + page, offset')
def test_list_pick_point_page_limit(CatalogsServiceAPI, CatalogsAPIHelper):
    imCode = 'at-lppf-weekends'

    filters = {
        'imCode': imCode,
        'filter.geoArea.leftTopCoords.latitude': 76.179344177246094,
        'filter.geoArea.leftTopCoords.longitude': 40.33099365234375,
        'filter.geoArea.rightBottomCoords.latitude': 45.179344177246094,
        'filter.geoArea.rightBottomCoords.longitude': 81.33099365234375,
        'page.limit': 10,
        'page.offset': 0
    }

    response = CatalogsServiceAPI.ListPickPoint(filters)
    response_json1 = response.json()

    with allure.step(f"Проверка что 10 результатов"):
        assert len(response_json1['points']) == 10, f"page.limit = 10, результатов должно быть 10"

    filters['page.offset'] = 1
    response = CatalogsServiceAPI.ListPickPoint(filters)
    response_json2 = response.json()

    with allure.step(f"Проверка что 10 результатов"):
        assert len(response_json2['points']) == 10, f"page.limit = 10, результатов должно быть 10"
    with allure.step(f"Проверка, что offset ответа сместился на 1"):
        assert response_json1['points'][1] == response_json2['points'][0],\
            f"Второй элемент должен быть равен первом предыдущего запроса"
