import allure
import pytest
from datetime import date
import datetime


@allure.feature('Catalogs API')
@allure.story('Получение расписания работы и загруженности отделения на заданную дату')
@allure.title('Получение расписания работы и загруженности отделения на заданную дату')
@pytest.mark.parametrize("pointCode, check_date", [
    (19728, date.today()),
    (19728, date.today() + datetime.timedelta(days=1)),
    (19728, date.today() + datetime.timedelta(days=2)),
    (19728, date.today() + datetime.timedelta(days=3)),
    (19728, date.today() + datetime.timedelta(days=4)),
    (19728, date.today() + datetime.timedelta(days=5)),
    (19728, date.today() + datetime.timedelta(days=6)),
])
def test_work_load(CatalogsServiceAPI, CatalogsAPIHelper, array_helper, pointCode, check_date):
    date_string = check_date.strftime("%Y-%m-%d")
    weekday_string = check_date.strftime("%A").upper()
    weekday_num = check_date.weekday()

    expected_result = {
        "day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": weekday_string, "date": '2020-09-13'}}

    if weekday_num == 5:
        expected_result = {
            "day": {"workStart": "11:00:00", "workEnd": "18:00:00", "weekDay": weekday_string, "date": '2020-09-13'}}
    if weekday_num == 6:
        expected_result = {
            "day": {"weekDay": weekday_string, "date": '2020-09-13'}}

    with allure.step(f"Получение расписания для отделения {pointCode}, день {date_string} ({weekday_num})"):
        response = CatalogsServiceAPI.GetWorkForDate({'pointCode': pointCode, 'date': date_string})
        CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

    response_body = response.json()
    with allure.step('Проверка ответа'):
        with allure.step('Проверка что ответ не пустой'):
            assert response_body is not {}
        array_helper.assert_arrays(response_body, expected_result)
