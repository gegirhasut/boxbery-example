# https://confluence.boxberry.ru/pages/viewpage.action?pageId=13305485
# https://middle-catalogs-stage.boxberry.ru/docs/
import allure
import pytest
from library.common.db import *


@allure.feature('Catalogs API')
@allure.story('Поиск отделений (pickupPoint + postomat)')
@allure.title('Поиск отделений (pickupPoint + postomat)')
def test_FindPoints_pickup_point_postomat_catalog(CatalogsServiceAPI, CatalogsAPIHelper):
    with allure.step('Подготовка тестовых данных'):
        db = DBClient('catalog')
        db.update(f"UPDATE pickpoints SET status = 2, pickup_point = 't', postomat ='f' WHERE point_code='19979'")
        db.update(f"UPDATE pickpoints SET status = 2, pickup_point = 'f', postomat ='t' WHERE point_code='19008'")
        db.update(f"UPDATE pickpoints SET status = 2, pickup_point = 'f', postomat ='f' WHERE point_code='19027'")

    with allure.step('Проверка запроса с pickupPoint = true'):
        response = CatalogsServiceAPI.FindPoints({'cityCode': 68, 'typesPoints.pickupPoint': 'true'})
        CatalogsAPIHelper.CheckResponse200(response)

        response_body = response.json()
        with allure.step('Проверка результатов'):
            with allure.step(f"Проверка количества отделений > 0"):
                assert len(response_body['points']) > 0
            with allure.step(f"Проверка наличия отделения 19979 в списке"):
                assert '19979' in response_body['points'], "Отделение 19979 не найдено"
            with allure.step(f"Проверка отсутствия отделения 19008 в списке"):
                assert '19008' not in response_body['points'], "Отделение 19008 должно отсутствовать"
            with allure.step(f"Проверка отсутствия отделения 19027 в списке"):
                assert '19027' not in response_body['points'], "Отделение 19027 должно отсутствовать"

    with allure.step('Проверка запроса с pickupPoint = false'):
        response = CatalogsServiceAPI.FindPoints({'cityCode': 68, 'typesPoints.pickupPoint': 'false'})
        CatalogsAPIHelper.CheckResponse200(response)

        response_body = response.json()
        with allure.step('Проверка результатов'):
            with allure.step(f"Проверка количества отделений > 0"):
                assert len(response_body['points']) > 0
            with allure.step(f"Проверка наличия отделения 19008 в списке"):
                assert '19008' in response_body['points'], "Отделение 19008 не найдено"
            with allure.step(f"Проверка наличия отделения 19027 в списке"):
                assert '19027' in response_body['points'], "Отделение 19027 не найдено"
            with allure.step(f"Проверка отсутствия отделения 19979 в списке"):
                assert '19979' not in response_body['points'], "Отделение 19979 должно отсутствовать"


@allure.feature('Catalogs API')
@allure.story('Ошибка при Поиске отделений без указания фильтров')
@allure.title('Ошибка при поиске отделений без указания фильтров')
def test_no_filter_error_invalid_arguments(CatalogsServiceAPI, CatalogsAPIHelper):
    with allure.step('Поиск отделений без фильтров'):
        response = CatalogsServiceAPI.FindPoints()
    CatalogsAPIHelper.CheckResponse400HasInvalidArguments(response)


@allure.feature('Catalogs API')
@allure.story('Пустой результат при поиске отделения по городу где его нет')
@allure.title('Пустой результат при поиске отделения по городу где его нет')
def test_empty_result(CatalogsServiceAPI, CatalogsAPIHelper):
    with allure.step('Поиск отделений по cityCode=Н00168975 (Абазовка, Украина)'):
        response = CatalogsServiceAPI.FindPoints({'cityCode': 'Н00168975'})
    CatalogsAPIHelper.CheckResponse200EmptyJson(response)


@allure.feature('Catalogs API')
@allure.story('Поиск отделений по заданным параметрам')
@allure.title('Поиск отделений по заданным параметрам')
@pytest.mark.parametrize("filters, points, pointsAbsence, updates", [
    ({'cityCode': 68}, {'00118', 'ПП-7701-169'}, {}, {}),
    ({'cityCode': 68, 'typesPoints.pickupPoint': 'true'}, {'00118', '10.013'}, {},
     {f"pickup_point = 't', postomat = 'f' WHERE point_code IN ('00118', '10.013')"},
     ),
    ({'cityCode': 68, 'typesPoints.pickupPoint': 'false'}, {'ПП-7705-316', 'ПП-7705-096'}, {},
     {f"pickup_point = 'f', postomat = 'f' WHERE point_code IN ('ПП-7705-316', 'ПП-7705-096')"},
     ),
    ({'cityCode': 68, 'typesPoints.postomat': 'true'}, {'ПП-7705-316', 'ПП-7705-096'}, {},
     {f"postomat = 't', pickup_point = 'f' WHERE point_code IN ('ПП-7705-316', 'ПП-7705-096')"},),
    ({'cityCode': 68, 'typesPoints.postomat': 'false'}, {'00118', '19962'}, {},
     {f"postomat = 'f', pickup_point = 'f' WHERE point_code IN ('00118', '19962')"}
     ),
    ({'cityCode': 68, 'properties.payment': 'true'}, {'00118', '10.013'}, {},
     {f"cash_payment = 't' WHERE point_code IN ('00118', '10.013')"}
     ),
    ({'cityCode': 68, 'properties.payment': 'false'}, {'ПП-7705-316', 'ПП-7705-096'}, {},
     {f"cash_payment = 'f' WHERE point_code IN ('ПП-7705-316', 'ПП-7705-096')"}
     ),
    ({'cityCode': 68, 'properties.enablePartialDelivery': 'true'}, {'00118', '10.013'}, {},
     {f"enable_partial_delivery = 't' WHERE point_code IN ('00118', '10.013')"}),
    ({'cityCode': 68, 'properties.enablePartialDelivery': 'false'}, {'ПП-7705-316', 'ПП-7705-096'}, {},
     {f"enable_partial_delivery = 'f' WHERE point_code IN ('ПП-7705-316', 'ПП-7705-096')"}),
    ({'cityCode': 68, 'properties.enableOpening': 'true'}, {'00118', '10.013'}, {},
     {f"enable_opening = 't' WHERE point_code IN ('00118', '10.013', 'ПП-7705-316')"}),
    ({'cityCode': 68, 'properties.enableOpening': 'false'}, {'ПП-7701-234', 'ПП-7705-096'}, {'ПП-7705-316'},
     {f"enable_opening = 'f' WHERE point_code IN ('ПП-7701-234', 'ПП-7705-096')"}),
    ({'cityCode': 68, 'properties.fitting': 0}, {'00118', '10.013'}, {},
     {f"fitting = '0' WHERE point_code IN ('00118', '10.013')"}),
    ({'cityCode': 68, 'properties.fitting': 1}, {'ПП-7705-316', 'ПП-7705-096'}, {},
     {f"fitting = '1' WHERE point_code IN ('ПП-7705-316', 'ПП-7705-096')"}),
    ({'cityCode': 68, 'properties.fitting': 2}, {'00118', '10.013'}, {},
     {f"fitting = '2' WHERE point_code IN ('00118', '10.013', 'ПП-7705-316')"}),
    ({'cityCode': 68, 'properties.typeBusiness': 1}, {'00118', '19917'}, {'ПП-7705-316'},
     {f"type_of_office = '1' WHERE point_code IN ('00118', '19917')"}),
    ({'cityCode': 68, 'properties.typeBusiness': 2}, {'00118', '19917'}, {},
     {f"type_of_office = '2' WHERE point_code IN ('00118', '19917')"}),
    ({'cityCode': 68, 'properties.weight': 1}, {'00118', '19917'}, {}, {}),
    ({'cityCode': 68, 'properties.weight': 2}, {'00118', '19917'}, {}, {}),
    ({'cityCode': 68, 'properties.weight': 1000}, {}, {}, {}),
])
def test_list_filter(CatalogsServiceAPI, CatalogsAPIHelper, filters, points, pointsAbsence, updates):
    with allure.step('Подготовка данных'):
        db = DBClient('catalog')
        for update in updates:
            db.update(f"UPDATE pickpoints SET status = 2, {update}")

    response = CatalogsServiceAPI.FindPoints(filters)
    CatalogsAPIHelper.CheckResponse200(response)

    response_body = response.json()
    with allure.step('Проверка результатов'):
        if len(points) > 0:
            with allure.step(f"Проверка количества отделений > 0"):
                assert len(response_body['points']) > 0
        else:
            with allure.step(f"Проверка что отделений не найдено"):
                assert response_body == {}

        for point in points:
            with allure.step(f"Проверка наличия отделения {point} в списке"):
                assert point in response_body['points']

        for point in pointsAbsence:
            with allure.step(f"Проверка отсутствия отделения {point} в списке"):
                assert point not in response_body['points']

