from library.common.db import *
from datetime import date
import datetime


@allure.feature('Catalogs API')
@allure.story('Проверка изменений')
@allure.title('Проверка изменений')
def test_work_changes(CatalogsServiceAPI, array_helper):
    pointCode = '96151'
    yesterday = (date.today() + datetime.timedelta(days=-1))
    yesterday_str = yesterday.strftime("%Y-%m-%d")
    yesterday_num = yesterday.weekday() + 1
    today = date.today()
    today_str = today.strftime("%Y-%m-%d")
    today_num = today.weekday() + 1
    day1 = (date.today() + datetime.timedelta(days=1))
    day1_str = day1.strftime("%Y-%m-%d")
    day1_num = day1.weekday() + 1
    day1_weekday_string = day1.strftime("%A").upper()
    day2 = (date.today() + datetime.timedelta(days=2))
    day2_str = day2.strftime("%Y-%m-%d")
    day2_num = day2.weekday() + 1
    day2_weekday_string = day2.strftime("%A").upper()

    with allure.step(f"Очистим тестовые данные расписания для отделения {pointCode}"):
        db = DBClient('catalog')
        db.delete(f"DELETE FROM schedules WHERE point_code = '{pointCode}' AND date_start >= '{yesterday_str}'")

    with allure.step(f"Добавить изменение расписание для {pointCode} (вчера, сегодня, завтра, послезавтра)"):
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','{yesterday_str}',{yesterday_num},'11:00:00','21:00:00');")
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','{today_str}',{today_num},'11:30:00','21:00:00');")
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','{day1_str}',{day1_num},'12:00:00','21:00:00');")
        db.insert(f"INSERT INTO schedules(point_code, temporary, date_start, week_day, work_start, work_end)"
                  f"VALUES ({pointCode},'f','{day2_str}',{day2_num},'12:30:00','21:00:00');")

    with allure.step(f"Проверка изменений расписания за 1 день"):
        response = CatalogsServiceAPI.GetWorkChanges(pointCode, 1)

        with allure.step('Проверка данных ответа'):
            response_body = response.json()
            expected_json = {
                "days":
                    [
                        {"workStart": "12:00:00", "workEnd": "21:00:00", "weekDay": day1_weekday_string, "date": day1_str}
                    ]
            }
            array_helper.assert_arrays(response_body, expected_json)

    with allure.step(f"Проверка изменений расписания за 2 дня"):
        response = CatalogsServiceAPI.GetWorkChanges(pointCode, 2)

        with allure.step('Проверка данных ответа'):
            response_body = response.json()
            expected_json = {
                "days":
                    [
                        {"workStart": "12:00:00", "workEnd": "21:00:00", "weekDay": day1_weekday_string, "date": day1_str},
                        {"workStart": "12:30:00", "workEnd": "21:00:00", "weekDay": day2_weekday_string, "date": day2_str}
                    ]
            }
            array_helper.assert_arrays(response_body, expected_json)

    with allure.step(f"Проверка изменений расписания за 3 дня"):
        response = CatalogsServiceAPI.GetWorkChanges(pointCode, 3)

        with allure.step('Проверка данных ответа'):
            response_body = response.json()
            expected_json = {
                "days":
                    [
                        {"workStart": "12:00:00", "workEnd": "21:00:00", "weekDay": day1_weekday_string, "date": day1_str},
                        {"workStart": "12:30:00", "workEnd": "21:00:00", "weekDay": day2_weekday_string, "date": day2_str}
                    ]
            }
            array_helper.assert_arrays(response_body, expected_json)

    db.delete(f"DELETE FROM schedules WHERE point_code = '{pointCode}' AND date_start >= '{yesterday_str}'")
