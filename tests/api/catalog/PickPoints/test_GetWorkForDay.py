import pytest
from library.common.db import *


@allure.feature('Catalogs API')
@allure.story('Получение расписания работы и загруженности отделения в заданный день недели')
@allure.title('Получение расписания работы и загруженности отделения в заданный день недели')
@pytest.mark.parametrize("pointCode, day, expected_json", [
    (19728, 1, {"day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": "MONDAY", "date": "2020-09-13"}}),
    (19728, 2, {"day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": "TUESDAY", "date": "2020-09-13"}}),
    (19728, 3, {"day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": "WEDNESDAY", "date": "2020-09-13"}}),
    (19728, 4, {"day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": "THURSDAY", "date": "2020-09-13"}}),
    (19728, 5, {"day": {"workStart": "10:00:00", "workEnd": "21:00:00", "weekDay": "FRIDAY", "date": "2020-09-13"}}),
    (19728, 6, {"day": {"workStart": "11:00:00", "workEnd": "18:00:00", "weekDay": "SATURDAY", "date": "2020-09-13"}}),
    (19728, 7, {"day": {"weekDay": "SUNDAY", "date": "2020-09-13"}}),
])
def test_work_load_week(CatalogsServiceAPI, CatalogsAPIHelper, array_helper, pointCode, day, expected_json):
    response = CatalogsServiceAPI.GetWorkForDay({'pointCode': pointCode, 'day': day})
    CatalogsAPIHelper.CheckResponse200NotEmptyJson(response)

    with allure.step('Проверка данных ответа'):
        response_body = response.json()
        array_helper.assert_arrays(response_body, expected_json)


@allure.feature('Catalogs API')
@allure.story('Проверка hourlyLoad')
@allure.title('Проверка hourlyLoad')
def test_hourlyLoad(CatalogsServiceAPI, array_helper):
    pointCode = '96241'

    with allure.step(f"Очистим данные workloads для отделения {pointCode}"):
        db = DBClient('catalog')
        db.delete(f"DELETE FROM workloads WHERE point_code = '{pointCode}'")

    with allure.step(f"Добавить hourlyLoad для {pointCode}, день 1"):
        db.insert(f"INSERT INTO workloads(point_code,day_of_week,hour_of_day,load) VALUES ({pointCode},1,13,2)")
        db.insert(f"INSERT INTO workloads(point_code,day_of_week,hour_of_day,load) VALUES ({pointCode},1,14,3)")
        db.insert(f"INSERT INTO workloads(point_code,day_of_week,hour_of_day,load) VALUES ({pointCode},1,15,2)")

    with allure.step(f"Проверка дня 1, появились изменения"):
        response = CatalogsServiceAPI.GetWorkForDay({'pointCode': pointCode, 'day': 1})

        with allure.step('Проверка данных ответа'):
            response_body = response.json()
            assert "hourlyLoad" in response_body, f"hourlyLoad нет в ответе. Response: " + str(response_body)
            # сортируем часы в ответе, т.к. они приходят без сортировки и позиции меняются
            response_body["hourlyLoad"] = sorted(response_body["hourlyLoad"], key=lambda item: item['time'])
            expected_json = {
                "day": {"workStart": "12:00:00", "workEnd": "23:45:00", "weekDay": "MONDAY", "date": "2020-10-20"},
                "hourlyLoad": [
                    {"time": 13, "load": 2},
                    {"time": 14, "load": 3},
                    {"time": 15, "load": 2}
                ]
            }
            array_helper.assert_arrays(response_body, expected_json)

    with allure.step(f"Проверка дня 2, без изменений, hourlyLoad нет"):
        response = CatalogsServiceAPI.GetWorkForDay({'pointCode': pointCode, 'day': 2})

        with allure.step('Проверка данных ответа'):
            response_body = response.json()
            expected_json = {
                "day": {"workStart": "12:00:00", "workEnd": "23:45:00", "weekDay": "TUESDAY", "date": "2020-10-20"}}
            array_helper.assert_arrays(response_body, expected_json)

    db.delete(f"DELETE FROM workloads WHERE point_code = '{pointCode}'")


@allure.feature('Catalogs API')
@allure.story('Проверка параметра temporary')
@allure.title('Проверка параметра temporary')
def test_temporary_param(CatalogsServiceAPI, array_helper):
    pointCode = 96131

    with allure.step('Подготовка тестовых данных'):
        db = DBClient('catalog')
        db.update(
            f"UPDATE schedules SET temporary='f' WHERE point_code='{pointCode}' AND date_start='2020-10-19' AND week_day=6")

    response = CatalogsServiceAPI.GetWorkForDay({'pointCode': pointCode, 'day': 6})

    with allure.step('Проверка данных ответа'):
        response_body = response.json()
        expected_json = {
            "day": {"workStart": "11:00:00", "workEnd": "20:00:00", "weekDay": "SATURDAY", "date": "2020-10-19"}}
        array_helper.assert_arrays(response_body, expected_json)

    with allure.step('Делаем субботу temporary на дату 2020-10-19'):
        db.update(f"UPDATE schedules SET temporary='t' WHERE point_code='{pointCode}' AND date_start='2020-10-19' AND week_day=6")

    response = CatalogsServiceAPI.GetWorkForDay({'pointCode': pointCode, 'day': 6})

    with allure.step('Проверка данных ответа (вернулось от 13 сентября)'):
        response_body = response.json()
        expected_json = {
            "day": {"workStart": "11:00:00", "workEnd": "20:00:00", "weekDay": "SATURDAY", "date": "2020-09-13"}}
        array_helper.assert_arrays(response_body, expected_json)

    db.update(f"UPDATE schedules SET temporary='f' WHERE point_code='{pointCode}' AND date_start='2020-10-19' AND week_day=6")