from library.ui.lkim.HomePage import *
from library.ui.lkim.CabinetPage import *
from library.ui.lkim.ResetPasswordPage import *


@allure.feature('ЛК ИМ')
@allure.story('Согласие со сбором куки')
@allure.title('Согласие со сбором куки')
def test_cookie_agreement(page):
    homePage = HomePage(page)
    homePage.\
        openAndApproveCookieAgreement().\
        reload().\
        waitForSelector(HomePage.cookieElement, state='hidden')


@allure.feature('ЛК ИМ')
@allure.story('Успешный вход')
@allure.title('Успешный вход')
def test_login(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement()
    homePage.login('m_popelnitskiy@boxberry.ru', 'Hy2ebjw-IT')

    notification = page.textContent('.register_info_notification')
    notification = " ".join(notification.split())
    assert notification == 'Для завершения регистрации и возможности использовать сервисы API, Вам нужно заполнить информацию о себе',\
        f"Проверка нотификации у пользователя с незавершенной регистрацией"
    page.waitForSelector("#js-registration-form", state='visible')
    h1 = page.textContent('#js-registration-form h1')
    assert h1 == 'Регистрационные данные', f"Заголовок h1 не верный"
    page.waitForSelector('input[name="ip[inn]"]', state='visible')


@allure.feature('ЛК ИМ')
@allure.story('Успешная регистрация и логин')
@allure.title('Успешная регистрация и логин')
def test_reg_login(page, string_helper, lkim_helper):
    legalName = 'Название: ' + string_helper.get_random_string()
    fio = 'ФИО ' + string_helper.get_random_string()
    phone = string_helper.get_random_phone()
    email = 'lkim-autotest' + string_helper.get_random_email()

    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement().register(legalName, fio, phone, email)
    cabinet = CabinetPage(page)
    cabinet.logout()

    password = lkim_helper.findUserPass(email, 12)

    homePage.login(email, password)


@allure.feature('ЛК ИМ')
@allure.story('Ошибки формы регистрации')
@allure.title('Ошибки формы регистрации')
def test_reg_errors(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement().register(shouldBeSuccess=False)

    assert "Необходимо заполнить «Наименование клиента (юридическое)»." == homePage.getLegalNameError()
    assert "Необходимо заполнить «Наименование клиента»." == homePage.getFioError()
    assert "Необходимо заполнить «Телефон»." == homePage.getPhoneError()
    assert "Необходимо заполнить «Email»." == homePage.getEmailError()


@allure.feature('ЛК ИМ')
@allure.story('Ошибки формы регистрации: емайл и телефон')
@allure.title('Ошибки формы регистрации: емайл и телефон')
def test_reg_errors_email_phone(page, string_helper):
    legalName = 'Название: ' + string_helper.get_random_string()
    fio = 'ФИО ' + string_helper.get_random_string()
    phone = string_helper.get_random_phone(5)
    email = 'lkim-autotest'

    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement().register(legalName, fio, phone, email, shouldBeSuccess=False)

    assert "«Телефон» должен содержать 11 цифр." == homePage.getPhoneError()
    assert 'Значение "lkim-autotest" не является правильным email адресом' == homePage.getEmailError()


@allure.feature('ЛК ИМ')
@allure.story('Регистрация без подтверждения условий работы')
@allure.title('Регистрация без подтверждения условий работы')
def test_reg_no_agreement(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement().register(agreement=False, shouldBeSuccess=False)
    assert homePage.innerText('#bxb-message') == 'Вы должны согласиться с условиями обработки персональных данных'
    homePage.closeBXBModal()


@allure.feature('ЛК ИМ')
@allure.story('Просмотр сообщения об обработке данных')
@allure.title('Просмотр сообщения об обработке данных')
def test_reg_agreement_message(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement().switchToRegTab()
    homePage.click('.js-agreement').waitForSelector('#personalData.modal.fade.in')

    assert homePage.innerText('#personalDataTitle') == 'Согласие на обработку персональных данных'


@allure.feature('ЛК ИМ')
@allure.story('Не верный пароль')
@allure.title('Не верный пароль')
def test_login_wrong_pass(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement()
    homePage.login('m_popelnitskiy@boxberry.ru', 'wrong_password', False)

    assert homePage.innerText('#bxb-message') == 'Неверный логин или пароль'


@allure.feature('ЛК ИМ')
@allure.story('Восстановление пароля')
@allure.title('Восстановление пароля')
def test_login_password_reset(page, lkim_helper, string_helper):
    email = 'passwordreset@boxberry.com'
    newPass = string_helper.get_random_string()

    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement()
    homePage.openPasswordReset()

    resetPassPage = ResetPasswordPage(page)
    resetPassPage.sendResetLink(email)
    resetLink = lkim_helper.findResetPasswordLink(email, 12)
    resetPassPage.openResetPassLink(resetLink).resetPass(newPass)

    assert 'Пароль успешно обновлен.' in resetPassPage.innerText('.password-recovery__label')

    resetPassPage.clickToMainPage()
    homePage.login(email, newPass)