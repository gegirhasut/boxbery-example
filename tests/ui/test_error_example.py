from library.ui.lkim.HomePage import *


@allure.feature('UI test')
@allure.story('Пример упавшего теста со скриншотом')
@allure.title('Пример упавшего теста со скриншотом')
def test_failed_example(page):
    homePage = HomePage(page)
    homePage.openAndApproveCookieAgreement()
    homePage.click('.button-not-exists', 'Кнопка, которой нет')
