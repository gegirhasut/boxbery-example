<?php
if (isset($_REQUEST['info'])) {
    phpinfo();exit;
}
$config = [
    'host' => 'postgres',
    'user' => 'postgres',
    'password' => 'postgres',
];

if (file_exists('psql-cfg.php')) {
    require_once('psql-cfg.php');
}

if (!isset($_REQUEST['sql'])) {
    echo "Define SQL query";exit;
}
if (!isset($_REQUEST['db'])) {
    echo "Define DB";exit;
}
if (isset($_REQUEST['host'])) {
    $config['host'] = $_REQUEST['host'];
}
if (isset($_REQUEST['user'])) {
    $config['host'] = $_REQUEST['user'];
}
if (isset($_REQUEST['password'])) {
    $config['host'] = $_REQUEST['password'];
}
$sql = $_REQUEST['sql'];
$db = $_REQUEST['db'];

$conn_string_pdo = "pgsql:host={$config['host']};port=5432;dbname={$db};user={$config['user']};password={$config['password']}";
$conn = new PDO($conn_string_pdo);

if (!$conn) {
    echo "Unable to connect to DB.\n";
    exit;
}

$result = $conn->query($sql);

if (strpos(strtolower($sql), 'select') !== false) {
    if (isset($_REQUEST['assoc'])) {
        $result = $result->fetchAll(PDO::FETCH_ASSOC);
    } else {
        $result = $result->fetchAll(PDO::FETCH_NUM);
    }
}

if (!$result) {
    echo "An error occurred executing $sql.\n";
    exit;
}

$rows = [];
foreach ($result as $row) {
    $rows[] = $row;
}

echo json_encode($rows, JSON_UNESCAPED_UNICODE);