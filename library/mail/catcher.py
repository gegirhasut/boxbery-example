from library.common.api import *
import time


class Mailcatcher:
    timeout = 30
    base_address = f"http://mail.docker.boxberry.ru/"

    def __init__(self):
        self.api = ApiClient(base_address=self.base_address)

    def getLastMessageNum(self):
        allMess = self.getAll()
        lastId = allMess[len(allMess) - 1]['id']
        return lastId

    def lastMessageUrl(self, email, topic, lastNum, checks=12):
        message = self.findLastMessage(email, topic, lastNum, checks)
        return None if message is None else self.base_address + f"messages/{message['id']}.html"

    def lastMessageHtml(self, email):
        message = self.findLastMessage(email)
        return None if message is None else self.readEmail(message['id'])

    def findLastMessage(self, email, topic='Топик не задан!', lastNum=0, times=12):
        print(f"Looking for message of {email}, {topic}, til {lastNum}")
        for x in range(times):
            print(f"Attempt {x}")
            allMess = self.getAll()
            print(f"Emails found {len(allMess)}")
            print(f"First is {len(allMess) - 1} : {allMess[len(allMess) - 1]['recipients'][0]}")
            for i in range(len(allMess) - 1, -1, -1):
                print(f"Checking i = {i}, id = {allMess[i]['id']}")
                if allMess[i]['id'] <= lastNum:
                    print(f"Stop when {i} <= {lastNum}")
                    break

                if allMess[i]['recipients'][0] == f"<{email}>":
                    if allMess[i]['subject'] == topic:
                        print(f"Found in {i}")
                        return allMess[i]
                    else:
                        print(f"{i}, another topic found")
                        #Только первое найденое
                        break
            #print(f"Email not found {email}")
            print(f"Attempt {x}, not found. Try again after 5 seconds.")
            time.sleep(self.timeout)

        return None

    def readEmail(self, id):
        return self.api.get(f"messages/{id}.html").content

    def getAll(self):
        response = self.api.get("messages")
        allure.attach(str(response.content), "/messages", allure.attachment_type.JSON)
        return response.json()
