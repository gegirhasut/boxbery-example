import os
import allure
from allure_commons.types import AttachmentType


class BaseUI:
    pageElement = ""
    imgIndex = 0

    def __init__(self, page, baseUrl=None):
        self.page = page
        self.baseUrl = baseUrl

    def goto(self, url):
        with allure.step(f"Открыть урл: {url}"):
            self.page.goto(url)

        return self

    def goBack(self):
        self.page.goBack()
        return self

    def open(self):
        with allure.step(f"Открыть страницу {self.baseUrl}"):
            self.page.goto(self.baseUrl)

            if self.pageElement != '':
                self.page.waitForSelector(self.pageElement, state='visible')

        return self

    def waitForSelector(self, css, state='visible', elName=None):
        mess = f"Ожидаем css = {css}, state = {state}" if elName is None else f"Ожидаем {elName}, css = {css}, state = {state}"
        with allure.step(mess):
            try:
                self.page.waitForSelector(css, state=state)
            except:
                print(f"Unable to wait selector = {css}, state = {state}")
                raise

        return self

    def click(self, css, elName=None, force=True):
        mess = f"Нажать элемент, css = {css}" if elName is None else f"Нажать [{elName}], css = {css}"
        with allure.step(mess):
            if not force:
                self.waitForSelector(css)
            try:
                self.page.click(css, force=force)
            except:
                print(f"Unable to click selector = {css}, force = {force}")
                raise

        return self

    def fill(self, css, value, elName=None):
        mess = f"Заполнить [элемент]=[{value}], css = {css}" if elName is None else f"Заполнить [{elName}]=[{value}], css = {css}"
        with allure.step(mess):
            self.page.fill(css, value)
            return self

    def innerText(self, css):
        self.waitForSelector(css)
        return self.page.innerText(css)

    def reload(self):
        self.page.reload()
        return self

    def screenshot(self, name=None):
        if name is None:
            path = f"images/img_{os.environ.get('PYTEST_CURRENT_TEST').split(':')[-1].split(' ')[0]}_{self.imgIndex}.png"
            img = self.page.screenshot(path=path)
            allure.attach(img, path, attachment_type=AttachmentType.PNG)
        else:
            path = f"images/img_{name}_{self.imgIndex}.png"
            img = self.page.screenshot(path=path)
            allure.attach(img, name=path, attachment_type=AttachmentType.PNG)
        self.imgIndex = self.imgIndex + 1
