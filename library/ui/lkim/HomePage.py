from library.ui.lkim.Base import Base
import allure
import time


class HomePage(Base):
    baseUrl = "/"
    pageElement = ".auth-page"
    cookieElement = "#agreement-cookie"
    cookieApproveBtn = ".js-agreement-cookie"

    def __init__(self, page):
        super().__init__(page, self.baseUrl)

    def openAndApproveCookieAgreement(self):
        with allure.step(f"Открыть ЛК ИМ, принять куки"):
            self.open().click(self.cookieApproveBtn, "Солгасен").waitForSelector(self.cookieElement, 'hidden')

        return self

    def login(self, email, password, success=True):
        with allure.step(f"Логин ({email}, {password})"):
            self.fill('#authorizationform-email', email)
            self.fill('#authorizationform-password', password)
            self.click(".js-login-btn")
            if success:
                self.waitForSelector(".loading-page.web")
                self.waitForSelector(".register_info_notification", state='visible')
            else:
                self.waitForSelector('#bxbModal', 'visible', 'Модальное окно с ошибкой')

        return self

    def switchToRegTab(self):
        with allure.step(f"Переключить на регистрацию"):
            self.click('div[data-tab="register"]')
            self.waitForSelector('#register-form')

        return self

    def openPasswordReset(self):
        with allure.step(f"Нажать Забыли пароль?"):
            self.click('.login-reg__link')
            self.waitForSelector('#passwordresetrequestform-email')

        return self

    def register(self, legalName=None, fio=None, phone=None, email=None, site=None, agreement=None, shouldBeSuccess=True):
        self.switchToRegTab()

        with allure.step(f"Регистрация"):

            with allure.step(f"Заполнение формы"):
                if legalName is not None:
                    self.fill('#registerform-name_legal', legalName, 'Юридическое имя')
                if fio is not None:
                    self.fill('#registerform-name', fio, 'ФИО')
                if phone is not None:
                    self.fill('#registerform-phone', phone, 'Телефон')
                if email is not None:
                    self.fill('#registerform-email', email, 'Е-майл')
                if site is not None:
                    self.fill('#registerform-url', site, 'Сайт')

            if agreement is False:
                with allure.step(f"Отключить соглашение"):
                    self.click('.jq-checkbox')

            with allure.step(f"Жмем регистрация"):
                self.click('#login-page-register-button')

                if shouldBeSuccess:
                    self.waitForSelector(".loading-page.web")
                    self.waitForSelector('.register_info_notification')
                else:
                    self.waitForSelector('#register-form .field-error:not(:empty), .modal.fade.in')

                time.sleep(1)

        return self

    def getLegalNameError(self):
        with allure.step(f"Получить ошибку легального имени"):
            return self.innerText('.field-registerform-name_legal .field-error')

    def getFioError(self):
        with allure.step(f"Получить ошибку в ФИО"):
            return self.innerText('.field-registerform-name .field-error')

    def getPhoneError(self):
        with allure.step(f"Получить ошибку в телефоне"):
            return self.innerText('.field-registerform-phone .field-error')

    def getEmailError(self):
        with allure.step(f"Получить ошибку в емайл"):
            return self.innerText('.field-registerform-email .field-error')
