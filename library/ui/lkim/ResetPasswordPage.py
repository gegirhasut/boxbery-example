from library.ui.lkim.Base import Base
from library.ui.lkim.Base import Base
import allure


class ResetPasswordPage(Base):
    baseUrl = "/password-reset/request"
    pageElement = ".register_info_notification"

    def __init__(self, page):
        super().__init__(page, self.baseUrl)

    def sendResetLink(self, email):
        with allure.step(f"Восстановить для {email}"):
            self.fill('#passwordresetrequestform-email', email)
            self.click('.password-recovery__btn button')
            self.waitForSelector('.hint-block', 'visible')

        return self

    def openResetPassLink(self, url):
        with allure.step(f"Переходим по ссылке восстановления {url}"):
            self.goto(url)
            self.waitForSelector('#passwordresetform-password')

        return self

    def resetPass(self, password):
        with allure.step(f"Меняем пароль на {password}"):
            self.fill('#passwordresetform-password', password)
            self.fill('#passwordresetform-passwordrepeat', password)
            self.click('.password-recovery__btn button')
            self.waitForSelector("a[href='/site/index']")

        return self

    def clickToMainPage(self):
        with allure.step(f"Нажать ссылку 'войти'"):
            self.click('.password-recovery__label a')
            self.waitForSelector('#authorizationform-email')

        return self