from library.ui.BaseUI import BaseUI
import allure
import time


class Base(BaseUI):
    def __init__(self, page=None, baseUrl=None):
        super().__init__(page, 'https://test-ecom.boxberry.ru' + baseUrl)

    def closeBXBModal(self):
        with allure.step(f"Закрыть модальное окно"):
            self.click('.modal.fade.in .js-bxb-close-btn')
            self.waitForSelector('#bxbModal.modal.fade:not(.in)[style="display: none;"]', 'hidden')
            #self.screenshot()
            time.sleep(1)

        return self
