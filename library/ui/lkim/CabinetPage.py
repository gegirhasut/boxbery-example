from library.ui.lkim.Base import Base
import allure


class CabinetPage(Base):
    baseUrl = "register/index"
    pageElement = ".register_info_notification"

    def __init__(self, page):
        super().__init__(page, self.baseUrl)

    def logout(self):
        with allure.step(f"Логаут"):
            self.click('.header_user .icon-arr-down')
            self.click('.header_user_drop[style="display: block;"] .li-logout')
            self.waitForSelector('#login-form')
            #self.screenshot()

        return self