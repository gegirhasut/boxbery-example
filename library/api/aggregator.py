# https://confluence.boxberry.ru/pages/viewpage.action?pageId=13305485
# https://middle-catalogs-stage.boxberry.ru/docs/
# INSERT INTO "contract_services" ("create_date", "update_date", "version_time", "contract_code", "service_code", "is_default", "service_type", "business_area_id", "payer_type_id", "start_date", "stop_date", "is_canceled", "percent", "service_price")
# VALUES (now(), now(), now(), 'BBR0012112', '000001417', '0', '1', '1', NULL, NOW() - INTERVAL '1 DAY', NULL, '0', NULL, NULL);
import pytest

from library.api._base_service import BaseService
from library.common.api import *


class AggregatorService(BaseService):
    def __init__(self):
        super().__init__('aggregator')

    def GetCities(self, filters=None, client=None, query=None):
        with allure.step('Запрос списка городов'):
            if query is None:
                filterParams = self.get_url_by_params(filters, '')
            else:
                filterParams = query
            if client is None:
                return self.api_client.get("cities" + filterParams)
            else:
                return self.api_client.get("cities" + filterParams, None, {'Authorization': f"Bearer {client}"})

    def GetCountries(self, filters=None, client=None, query=None):
        with allure.step('Запрос списка стран'):
            if query is None:
                filterParams = self.get_url_by_params(filters, '')
            else:
                filterParams = query
            if client is None:
                return self.api_client.get("countries" + filterParams)
            else:
                return self.api_client.get("countries" + filterParams, None, {'Authorization': f"Bearer {client}"})

    def FindPoints(self, filters=None):
        with allure.step('Поиск отделений по заданным параметрам'):
            filterParams = self.get_url_by_params(filters, '')
            return self.api_client.get("pickpoint/find" + filterParams)

    def GetContractInfo(self, filters=None, client=None, query=None):
        with allure.step('Получение информации о юр лицах'):
            if query is None:
                filterParams = self.get_url_by_params(filters, 'filters.')
            else:
                filterParams = query
            if client is None:
                return self.api_client.get("contract/info" + filterParams)
            else:
                return self.api_client.get("contract/info" + filterParams, None, {'Authorization': f"Bearer {client}"})


@pytest.fixture
def AggregatorServiceApi():
    return AggregatorService()


class AggregatorApiAllureHelper(ApiAllureHelper):
    def CheckResponse200HasCities(self, response, checkFully=False):
        with allure.step('Проверка ответа на 200 и городов'):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия городов в списке'):
                assert 'cities' in response_body, f"В ответе нет cities"
                assert len(response_body["cities"]) > 0, f"В ответе пустой cities"
            if checkFully:
                with allure.step('Проверка параметров городов (countryCode, country, countryFullName ...)'):
                    for city in response_body["cities"]:
                        with allure.step(f"Проверка {city['name']}"):
                            allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                            assert len(city['name']) > 0, f"Пустое названия города (name)"
                            with allure.step(f"Проверка countryCode > 0"):
                                assert city['countryCode'] > 0, f"Пустое значение кода страны (countryCode)"
                            with allure.step(f"Проверка cityCode не пустая"):
                                assert len(city['cityCode']) > 0, f"Пустое значение кода города (cityCode)"
                            #with allure.step(f"Проверка region присутствует"):
                            #    assert 'region' in city
                            #with allure.step(f"Проверка region не пустая"):
                            #    assert len(city['region']) > 0


@pytest.fixture
def AggregatorApiHelper():
    return AggregatorApiAllureHelper()
