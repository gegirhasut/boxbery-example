# https://confluence.boxberry.ru/pages/viewpage.action?pageId=25591946
# https://middle-partner-stage.boxberry.ru/docs/
import pytest

from library.api._base_service import BaseService
from library.common.api import *


class PartnerService(BaseService):
    def __init__(self):
        super().__init__('partner')

    def CreateShop(self, params=None):
        with allure.step(f"Создание потребителя, тип {params['clientType']}"):
            paramsQuery = self.get_url_by_params(params, '')
            return self.api_client.get("partner/create" + paramsQuery)

    def CheckServiceShop(self, params={}):
        shop = params['partnerCode'] if 'partnerCode' in params else 'не задано'
        with allure.step(f"Запрос услуг для магазина {shop}"):
            paramsQuery = self.get_url_by_params(params, '')
            return self.api_client.get("partner/check-service-shop" + paramsQuery)

    def FindShop(self, params={}):
        with allure.step(f"Запрос: поиск магазинов на основании переданных критериев"):
            paramsQuery = self.get_url_by_params(params, '')
            return self.api_client.get("partner/shop/find" + paramsQuery)

    def GetShopSettings(self, clientCode):
        with allure.step(f"Запрос: GetShopSettings"):
            return self.api_client.get("partner/settings?clientCode=" + clientCode)

    def SetShopSettings(self, clientCode, filters):
        with allure.step(f"Запрос: SetShopSettings"):
            return self.api_client.post("partner/set-shop-settings", None, None, {'clientCode': clientCode, 'filters': filters})


@pytest.fixture
def PartnerServiceAPI():
    return PartnerService()


class PartnerApiAllureHelper(ApiAllureHelper):
    def CheckResponse200Created(self, response):
        self.CheckResponse200(response)
        response_body = response.json()
        with allure.step('Проверка статуса Контрагент успешно создан'):
            assert response_body["status"] == "Контрагент успешно создан", f"Нет текста Контрагент успешно создан в поле status"

    def CheckResponse500Exists(self, response):
        self.CheckStatusCode(response.status_code, 500)
        response_body = response.json()
        with allure.step('Проверка наличия ошибки settings for partner already exists'):
            assert response_body["error"] == "settings for partner already exists", f"Не верный error"
            assert response_body["code"] == 2, f"code не равен 2"


@pytest.fixture
def PartnerAPIHelper():
    return PartnerApiAllureHelper()
