from library.common.api import *
import os


class BaseService:
    def __init__(self, service):
        image_tag = os.environ.get('IMAGE_TAG')
        if image_tag is None or image_tag == 'master':
            self.api_client = ApiClient(base_address=f"https://middle-{service}-stage.boxberry.ru/v1/")
        else:
            self.api_client = ApiClient(base_address=f"https://{image_tag}-{service}.middle-dev.boxberry.ru/v1/")

    def get_url_by_params(self, filters, prefix='filter.'):
        if filters is None:
            filters = {}
        filterParams = ''
        for param in filters:
            if filters[param] is not None:
                filterParams += f"{prefix}{param}={filters[param]}" if filterParams == '' else f"&{prefix}{param}={filters[param]}"

        return '' if filterParams == '' else '?' + filterParams
