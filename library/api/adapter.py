# https://confluence.boxberry.ru/pages/viewpage.action?pageId=27362344
# https://middle-adapter-stage.boxberry.ru/docs/#operation/GetCities
import pytest

from library.api._base_service import BaseService
from library.common.api import *


class AdapterService(BaseService):
    def __init__(self):
        super().__init__('adapter')

    def GetCities(self, filters=None, client=None, query=None):
        with allure.step('Запрос списка городов'):
            if query is None:
                filterParams = self.get_url_by_params(filters, '')
            else:
                filterParams = query
            if client is None:
                return self.api_client.get("city/list/cities" + filterParams)
            else:
                return self.api_client.get("city/list/cities" + filterParams, None, {'Authorization': f"Bearer {client}"})


@pytest.fixture
def AdapterServiceApi():
    return AdapterService()


class AdapterApiAllureHelper(ApiAllureHelper):
    def CheckResponse200HasCities(self, response, checkFully=False):
        with allure.step('Проверка ответа на 200 и городов'):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия городов в списке'):
                assert 'cities' in response_body, f"В ответе нет cities"
                assert len(response_body["cities"]) > 0, f"В ответе пустой cities"
            if checkFully:
                with allure.step('Проверка параметров городов (countryCode, country, countryFullName ...)'):
                    for city in response_body["cities"]:
                        with allure.step(f"Проверка {city['name']}"):
                            allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                            assert len(city['name']) > 0, f"Пустое названия города (name)"
                            with allure.step(f"Проверка countryCode > 0"):
                                assert city['countryCode'] > 0, f"Пустое значение кода страны (countryCode)"
                            with allure.step(f"Проверка cityCode не пустая"):
                                assert len(city['cityCode']) > 0, f"Пустое значение кода города (cityCode)"
                            #with allure.step(f"Проверка region присутствует"):
                            #    assert 'region' in city
                            #with allure.step(f"Проверка region не пустая"):
                            #    assert len(city['region']) > 0


@pytest.fixture
def AdapterApiHelper():
    return AdapterApiAllureHelper()
