# https://confluence.boxberry.ru/pages/viewpage.action?pageId=25591943
# https://middle-auth-stage.boxberry.ru/docs/
import pytest

from library.api._base_service import BaseService
from library.common.api import *
from library.common.db import *


class AuthService(BaseService):
    def __init__(self):
        super().__init__('auth')
        self.lastToken = None
        self.lastClientCode = None

    def AddToken(self, token, clientCode):
        with allure.step(f"Создание токена {token}, clientCode = {clientCode}"):
            self.lastToken = token
            self.lastClientCode = clientCode
            return self.api_client.post("token/add", json={"token": token, "clientCode": clientCode})

    def GetAccessToken(self, token, resource=None):
        if resource is None:
            with allure.step(f"Запросить AccessToken {token}"):
                return self.api_client.get("token/get-access?token=" + token)
        else:
            with allure.step(f"Запросить AccessToken {token} для ресурса {resource}"):
                return self.api_client.get("token/get-access?token=" + token + '&resource=' + resource)

    def ToggleToken(self, token, access):
        with allure.step(('Включение' if access else 'Отключение') + f" токена {token}"):
            return self.api_client.post("token/toggle", json={"token": token, "access": access})

    def ToggleClient(self, clientCode, blocked):
        with allure.step(('Блокировка' if blocked else 'Разблокировка') + f" клиента {clientCode}"):
            return self.api_client.post("token/toggle-client", json={"clientCode": clientCode, "blocked": blocked})


@pytest.fixture
def AuthServiceAPI():
    return AuthService()


class AuthApiAllureHelper(ApiAllureHelper):
    def CheckResonse403HasPermissionDenied(self, response):
        with allure.step(f"Проверить ответ на 403, access denied"):
            response_body = response.json()
            with allure.step('Проверка кода ответа 403'):
                assert response.status_code == 403
            with allure.step('Проверка на permissions denied'):
                assert response_body["error"] == 'permission denied'
                assert response_body["code"] == 7

    def CheckResponse404HasNoTokensAffected(self, response):
        with allure.step(f"Проверить ответ на 404, no tokens were affected"):
            response_body = response.json()
            with allure.step('Проверка кода ответа 404'):
                assert response.status_code == 404
            with allure.step('Проверка на no tokens were affected'):
                assert response_body["error"] == "no tokens were affected"
                assert response_body["code"] == 5

    def CheckResonse500HasUserWithTokenExists(self, response):
        with allure.step('Проверка ответа на 500, токен существует'):
            response_body = response.json()
            with allure.step('Проверка кода ответа 500'):
                assert response.status_code == 500
            with allure.step('Проверка на user with this token already exists'):
                assert response_body["error"] == "user with this token already exists"
                assert response_body["code"] == 2

    def CheckResponse200HasClientCode(self, response, expectedClientCode):
        with allure.step(f"Проверка ответа 200, есть клиентский код {expectedClientCode}"):
            self.CheckResponse200(response)
            response_body = response.json()
            clientCode = response_body["clientCode"]
            with allure.step('Проверка полученного clientCode'):
                assert clientCode == expectedClientCode

    def AddTokenToRole(self, token, role='master'):
        with allure.step(f"Добавляем токен {token} к роли {role}"):
            auth_db = DBClient('auth')
            with allure.step(f"Найти id токена"):
                token_id = auth_db.fetchOne(f"SELECT id FROM tokens WHERE token = '{token}'")['id']
                allure.attach(f"token id = {token_id}")
            with allure.step(f"Найти id роли"):
                role_id = auth_db.fetchOne(f"SELECT id FROM roles WHERE name = '{role}'")['id']
                allure.attach(f"role id = {role_id}")
            with allure.step(f"Добавить связь токена и роли"):
                auth_db.insert(f"INSERT INTO tokens_roles(role_id, token_id) VALUES({role_id}, {token_id})")

    def RemoveTokenToRole(self, token, role='master'):
        with allure.step(f"Удалить токен {token} из роли {role}"):
            auth_db = DBClient('auth')
            with allure.step(f"Найти id токена"):
                token_id = auth_db.fetchOne(f"SELECT id FROM tokens WHERE token = '{token}'")['id']
                allure.attach(f"token id = {token_id}")
            with allure.step(f"Найти id роли"):
                role_id = auth_db.fetchOne(f"SELECT id FROM roles WHERE name = '{role}'")['id']
                allure.attach(f"role id = {role_id}")
            with allure.step(f"Удалить связь токена и роли"):
                auth_db.delete(f"DELETE FROM tokens_roles WHERE role_id = {role_id} AND token_id = {token_id}")


@pytest.fixture
def AuthAPIHelper():
    return AuthApiAllureHelper()
