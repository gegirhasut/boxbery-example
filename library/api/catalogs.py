# https://confluence.boxberry.ru/pages/viewpage.action?pageId=13305485
# https://middle-catalogs-stage.boxberry.ru/docs/
import pytest

from library.api._base_service import BaseService
from library.common.api import *


class CatalogsService(BaseService):
    def __init__(self):
        super().__init__('catalogs')

    def ListCountry(self, filters=None):
        with allure.step('Запрос списка стран' + ('' if filters is None else ' с фильтром')):
            filterParams = self.get_url_by_params(filters)
            return self.api_client.get("country/list" + filterParams)

    def ListCity(self, filters=None):
        with allure.step('Запрос списка городов' + ('' if filters is None else ' с фильтром')):
            if isinstance(filters, str):
                filterParams = "?" + filters
            else:
                filterParams = self.get_url_by_params(filters)

            return self.api_client.get("city/list" + filterParams)

    def ListFilter(self):
        with allure.step('Запрос списка фильтров для ПВЗ'):
            return self.api_client.get("filter/list")

    def FindPoints(self, filters=None):
        with allure.step('Поиск отделений по заданным параметрам'):
            filterParams = self.get_url_by_params(filters, '')
            return self.api_client.get("pickpoint/find" + filterParams)

    def ListPickPoint(self, filters=None):
        with allure.step('Поиск отделений по заданным критериям'):
            filterParams = self.get_url_by_params(filters, '')
            return self.api_client.get("pickpoint/list" + filterParams)

    def GetWorkForDate(self, filters=None):
        with allure.step('Запросить расписание работы и загруженности отделения на заданную дату'):
            filterParams = self.get_url_by_params(filters, '')
            return self.api_client.get("pickpoint/work-for-date" + filterParams)

    def GetWorkForDay(self, filters=None):
        with allure.step(f"Запросить расписание работы и загруженности отделения {filters['pointCode']} в день недели ({filters['day']})"):
            filterParams = self.get_url_by_params(filters, '')
            return self.api_client.get("pickpoint/work-for-day" + filterParams)

    def GetWorkChanges(self, pointCode, numberDays):
        with allure.step(
                f"Получение информации об изменении графика работы отделения {pointCode} за будущие {numberDays} дней"):
            return self.api_client.get(f"pickpoint/work-changes?pointCode={pointCode}&numberDays={numberDays}")


@pytest.fixture
def CatalogsServiceAPI():
    return CatalogsService()


class CatalogsApiAllureHelper(ApiAllureHelper):
    def CheckResponse200HasCities(self, response, checkFully=False):
        with allure.step('Проверка ответа на 200 и городов'):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия городов в списке'):
                assert 'cities' in response_body, f"В ответе нет cities"
                assert len(response_body["cities"]) > 0, f"В ответе пустой cities"
            if checkFully:
                with allure.step('Проверка параметров городов (countryCode, country, countryFullName ...)'):
                    for city in response_body["cities"]:
                        with allure.step(f"Проверка {city['name']}"):
                            allure.attach(str(city), 'JSON DATA', allure.attachment_type.JSON)
                            assert len(city['name']) > 0, f"Пустое названия города (name)"
                            with allure.step(f"Проверка countryCode > 0"):
                                assert city['countryCode'] > 0, f"Пустое значение кода страны (countryCode)"
                            with allure.step(f"Проверка cityCode не пустая"):
                                assert len(city['cityCode']) > 0, f"Пустое значение кода города (cityCode)"
                            #with allure.step(f"Проверка region присутствует"):
                            #    assert 'region' in city
                            #with allure.step(f"Проверка region не пустая"):
                            #    assert len(city['region']) > 0

    def CheckResponse200HasCountries(self, response):
        with allure.step('Проверка ответа 200, проверка наличия данных по стран'):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия стран в списке'):
                assert 'countries' in response_body, f"В ответе не найдено countries"
                assert len(response_body["countries"]) > 0, f"В ответе пустой countries"
            with allure.step('Проверка параметров стран (countryCode, country, countryFullName ...)'):
                for country in response_body["countries"]:
                    with allure.step(f"Проверка {country['country']}"):
                        allure.attach(str(country), 'JSON DATA', allure.attachment_type.JSON)
                        assert len(country['country']) > 0
                        with allure.step(f"Проверка countryCode > 0"):
                            assert country['countryCode'] > 0

    def CheckResponse200WithCountry(self, response, country):
        with allure.step(f"Проверка ответа 200, проверка страны {country['country']}"):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия страны в ответе'):
                assert 'countries' in response_body, f"В ответе не найдено countries"
                assert len(response_body["countries"]) == 1, f"В ответе не 1 страна"
            with allure.step('Проверка данных по стране'):
                allure.attach(str(response_body["countries"][0]), 'Country JSON', allure.attachment_type.JSON)
                for param in country:
                    with allure.step(f"Проверить наличие {param}"):
                        assert param in response_body["countries"][0], f"Параметр {param} не найден"
                    with allure.step(f"{param} = {country[param]}"):
                        assert response_body["countries"][0][param] == country[param], f"Значение {param} != {country[param]}"

    def CheckResponse200WithCity(self, response, city):
        with allure.step(f"Проверка ответа 200, проверка города {city['name']}"):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка наличия города в ответе'):
                assert len(response_body["cities"]) == 1
            with allure.step('Проверка данных по городу'):
                for param in city:
                    with allure.step(f"{param} = {city[param]}"):
                        assert response_body["cities"][0][param] == city[param]

    def CheckResponse200HasNoCountries(self, response):
        with allure.step(f"Проверка ответа 200, пустой список стран"):
            self.CheckResponse200(response)
            response_body = response.json()
            with allure.step('Проверка что список стран пустой'):
                assert response_body == {}

    def CheckResponse400HasBadBoolValue(self, response):
        with allure.step(f"Проверка ответа 400, bad BoolValue: True"):
            response_body = response.json()
            with allure.step('Проверка кода ответа 400'):
                assert response.status_code == 400
            with allure.step('Проверка на bad BoolValue: True'):
                assert response_body["error"] == "bad BoolValue: True"
                assert response_body["code"] == 3

    def CheckResponse400HasInvalidArguments(self, response):
        with allure.step(f"Проверка ответа 400, invalid arguments"):
            response_body = response.json()
            with allure.step('Проверка кода ответа 400'):
                assert response.status_code == 400
            with allure.step('Проверка на invalid arguments'):
                assert response_body["error"] == "invalid arguments"
                assert response_body["code"] == 3

    def Countries(self, response):
        response_body = response.json()
        return response_body['countries']

    def Cities(self, response):
        response_body = response.json()
        return response_body['cities']


@pytest.fixture
def CatalogsAPIHelper():
    return CatalogsApiAllureHelper()
