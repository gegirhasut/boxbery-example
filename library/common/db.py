import psycopg2
import psycopg2.extras
from library.common.api import *
import allure
import os
import urllib.parse


class DBFactory:
    @staticmethod
    def connect(db_name=None):
        image_tag = os.environ.get('IMAGE_TAG')
        if image_tag is None or image_tag == 'master':
            db = DBClient(db_name)
            return db
        else:
            db = DBClientWrapper(image_tag, db_name)
            return db


class DBClientWrapper:
    connected_db = None
    conn = None
    base_address = None

    def __init__(self, image_tag=None, db_name=None):
        if db_name is not None:
            self.connect(image_tag, db_name)

    def connect(self, image_tag, db_name):
        with allure.step(f"Создаем соединение с базой {db_name} через db wrapper"):
            # https://ml-818-catalog-postgres.middle-dev.boxberry.ru/
            # https://{image_tag}-{db_name}-postgres.middle-dev.boxberry.ru/
            self.base_address = f"https://{image_tag}-{db_name}-postgres.middle-dev.boxberry.ru/psql.php?db={db_name}&assoc=1&sql="
            print('Use DB wrapper = ' + self.base_address)
            if self.connected_db != db_name:
                self.connected_db = db_name

        return self.conn

    def fetchOne(self, query):
        res = self.query(query)

        if res is not []:
            res = res[0]

        allure.attach(str(res), 'RECORD', allure.attachment_type.TEXT)

        return res

    def fetchAll(self, query):
        res = self.query(query)
        allure.attach(str(res), 'RECORDS', allure.attachment_type.TEXT)

        return res

    def query(self, query):
        with allure.step(f"{query}"):
            api_client = ApiClient(base_address=self.base_address)
            response = api_client.get(urllib.parse.quote_plus(query))

        return response.json()

    def insert(self, query):
        self.query(query)

    def update(self, query):
        self.query(query)

    def delete(self, query):
        self.query(query)

    def __del__(self):
        pass


class DBClient:
    connected_db = None
    conn = None

    def __init__(self, db_name=None):
        if db_name is not None:
            self.connect(db_name)

    def connect(self, db_name):
        with allure.step(f"Создаем соединение с базой {db_name}"):
            if self.connected_db != db_name:
                self.connected_db = db_name
                self.conn = psycopg2.connect(f"""
                            host=c-c9qu7vn7jt800c4kei44.rw.mdb.yandexcloud.net
                            port=6432
                            sslmode=verify-full
                            dbname={db_name}
                            user=autotests
                            password=sw0VROW0aEnVZNkqc7wW1XZo
                            target_session_attrs=read-write
                        """)

        return self.conn

    def fetchOne(self, query):
        with allure.step(f"{query}"):
            cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute(query)
            res = cursor.fetchone()
            cursor.close()
            res = dict(res)

            allure.attach(str(res), 'RECORD', allure.attachment_type.TEXT)

        return res

    def fetchAll(self, query):
        rows = []

        with allure.step(f"{query}"):
            cursor = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cursor.execute(query)
            res = cursor.fetchone()
            cursor.close()
            for row in res:
                rows.append(dict(row))

            allure.attach(str(rows), 'RECORDS', allure.attachment_type.TEXT)

        return rows

    def query(self, query):
        with allure.step(f"{query}"):
            cursor = self.conn.cursor()
            cursor.execute(query)
            cursor.close()

    def insert(self, query):
        with allure.step(f"{query}"):
            self.query(query)
            self.conn.commit()

    def update(self, query):
        with allure.step(f"{query}"):
            cursor = self.conn.cursor()
            cursor.execute(query)
            cursor.close()
            self.conn.commit()

    def delete(self, query):
        with allure.step(f"{query}"):
            self.query(query)
            self.conn.commit()

    def __del__(self):
        if self.conn is not None:
            self.conn.close()
