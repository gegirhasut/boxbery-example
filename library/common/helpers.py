import pytest
import random
import string
import allure


class StringHelper:
    def __init__(self):
        pass

    def get_random_string(self, length=20):
        letters = string.ascii_lowercase
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str

    def get_random_phone(self, length=10):
        letters = string.digits
        result_str = ''.join(random.choice(letters) for i in range(length))
        return result_str

    def get_random_email(self):
        result_str = self.get_random_string(7) + '@' + self.get_random_string(10) + '.com'
        return result_str


@pytest.fixture
def string_helper():
    return StringHelper()


class ArrayHelper:
    def __init__(self):
        pass

    # probably need to add checking of values, search by first property equivalent
    # check test_list_pickup_points:test_list_filter for reference
    def assert_arrays(self, arr, expected_arr):
        assert len(arr) == len(expected_arr)
        for param in expected_arr:
            with allure.step(f"Проверка наличия {param}"):
                assert param in arr, f"{param} не найден"
            if isinstance(expected_arr[param], dict):
                with allure.step(f"Проверка массива {param}"):
                    self.assert_arrays(arr[param], expected_arr[param]),\
                        f"Массивы {param} не равны. Result: " + str(arr) + ' Expected: ' + str(expected_arr)
            else:
                with allure.step(f"Проверка {param} = {expected_arr[param]}"):
                    assert arr[param] == expected_arr[param],\
                        f"{param} не равны. Result: " + str(arr) + ' Expected: ' + str(expected_arr)


@pytest.fixture
def array_helper():
    return ArrayHelper()