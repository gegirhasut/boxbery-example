from library.mail.catcher import *
from pyquery import PyQuery as pq
import pytest


class LKIMHelpers:
    def __init__(self):
        self.lastNum = None
        pass

    def init(self):
        with allure.step(f"Ищем номер последнего письма"):
            catcher = Mailcatcher()
            self.lastNum = catcher.getLastMessageNum()
            print(f"lastNum = {self.lastNum}")

    def findUserPass(self, email, tries=10):
        with allure.step(f"Поиск пароля в почте по email = {email}"):
            catcher = Mailcatcher()
            url = catcher.lastMessageUrl(email, 'Регистрация в службе доставки Boxberry', self.lastNum, tries)
            assert url is not None, f"Письмо с паролем не найдено {email} с {tries} попыток, поиск до {self.lastNum}"
            dom = pq(url)

        return dom('b:nth-of-type(2)').text()

    def findResetPasswordLink(self, email, tries=10):
        with allure.step(f"Поиск письма со ссылкой для восстановления пароля, email = {email}"):
            catcher = Mailcatcher()
            url = catcher.lastMessageUrl(email, 'Восстановление пароля для личного кабинета в службе доставки Boxberry', self.lastNum, tries)
            assert url is not None, f"Письмо со ссылкой на восстановление не найдено {email} с {tries} попыток, поиск до {self.lastNum}"
            dom = pq(url)

        return dom('a')[0].attrib['href']

@pytest.fixture
def lkim_helper():
    helper = LKIMHelpers()
    helper.init()
    return helper