import requests
import allure
import urllib3


class ApiClient:
    def __init__(self, base_address):
        self.base_address = base_address

    def post(self, path="/", params=None, data=None, json=None, headers=None):
        url = f"{self.base_address}{path}"
        with allure.step(f'POST: {url}'):
            allure.attach(str(json), 'POST DATA', allure.attachment_type.JSON)
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            response = requests.post(url=url, params=params, data=data, json=json, headers=headers, verify=False)
            allure.attach(response.content, 'RESPONSE ' + str(response.status_code), allure.attachment_type.JSON)
            return response

    def get(self, path="/", params=None, headers=None):
        url = f"{self.base_address}{path}"
        print("Url = " + url)
        with allure.step(f'GET: {url}'):
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            response = requests.get(url=url, params=params, headers=headers, verify=False)
            if len(response.content) < 1000:
                # too big content make very big artifacts in GitLab and pages are not published
                allure.attach(response.content, 'RESPONSE ' + str(response.status_code), allure.attachment_type.JSON)
            return response


class ApiAllureHelper:
    def __init__(self):
        pass

    def CheckStatusCode(self, code, expected_code):
        with allure.step(f"Проверка кода ответа {expected_code}"):
            assert code == expected_code, f"Код ответа запроса должен быть {expected_code}"

    def CheckResponse200(self, response):
        self.CheckStatusCode(response.status_code, 200)

    def CheckResponse200EmptyJson(self, response):
        with allure.step('Проверка ответа 200 и пустой JSON'):
            with allure.step(f"Проверка кода ответа {response.status_code} == 200?"):
                assert response.status_code == 200, f"Код ответа запроса должен быть 200"
            #allure.attach(str(response.json()), 'RESPONSE JSON', allure.attachment_type.JSON)
            with allure.step(f"Проверка ответа на пустой JSON"):
                assert response.json() == {}, f"Ответ должен быть пустным JSON"

    def CheckResponse200NotEmptyJson(self, response):
        with allure.step('Проверка ответа 200 и не пустой JSON'):
            with allure.step(f"Проверка кода ответа {response.status_code} == 200?"):
                assert response.status_code == 200, f"Код ответа запроса должен быть 200"
            #allure.attach(str(response.json()), 'RESPONSE JSON', allure.attachment_type.JSON)
            with allure.step(f"Проверка ответа на пустой JSON"):
                assert response.json() != {}, f"Ответ должен быть не пустным JSON"

    def CheckResponseCodeAndJson(self, response, code, json):
        with allure.step(f"Проверка ответа"):
            with allure.step(f"Проверка кода ответа {response.status_code} == {code}?"):
                assert response.status_code == code, f"Код ответа запроса должен быть {code}"
            #allure.attach(str(response.json()), 'RESPONSE JSON', allure.attachment_type.JSON)
            with allure.step(f"Проверка JSON ответа"):
                assert response.json() == json, f"Ответ должен быть равен {str(json)}"
